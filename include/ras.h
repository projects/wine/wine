/*
 * Copyright (C) 1998 Marcus Meissner
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WINE_RAS_H
#define __WINE_RAS_H

#include <lmcons.h>

#ifdef __cplusplus
extern "C" {
#endif
#include <pshpack4.h>
#include <inaddr.h>
#include <in6addr.h>

#define RAS_MaxCallbackNumber RAS_MaxPhoneNumber
#define RAS_MaxDeviceName     128
#define RAS_MaxDeviceType     16
#define RAS_MaxEntryName      256
#define RAS_MaxPhoneNumber    128
#define RAS_MaxAreaCode       10
#define RAS_MaxPadType        32
#define RAS_MaxX25Address     200
#define RAS_MaxFacilities     200
#define RAS_MaxUserData       200
#define RAS_MaxDnsSuffix      256

/* szDeviceType strings for RASDEVINFO */
#define RASDT_Direct     "direct"
#define RASDT_Modem      "modem"
#define RASDT_Isdn       "isdn"
#define RASDT_X25        "x25"
#define RASDT_Vpn        "vpn"
#define RASDT_Pad        "pad"
#define RASDT_Generic    "GENERIC"
#define RASDT_Serial     "SERIAL"
#define RASDT_FrameRelay "FRAMERELAY"
#define RASDT_Atm        "ATM"
#define RASDT_Sonet      "SONET"
#define RASDT_SW56       "SW56"
#define RASDT_Irda       "IRDA"
#define RASDT_Parallel   "PARALLEL"
#define RASDT_PPPoE      "PPPoE"

/* RAS errors */
#define RASBASE                                 600
#define PENDING                                 (RASBASE+0)
#define ERROR_INVALID_PORT_HANDLE               (RASBASE+1)
#define ERROR_PORT_ALREADY_OPEN                 (RASBASE+2)
#define ERROR_BUFFER_TOO_SMALL                  (RASBASE+3)
#define ERROR_WRONG_INFO_SPECIFIED              (RASBASE+4)
#define ERROR_CANNOT_SET_PORT_INFO              (RASBASE+5)
#define ERROR_PORT_NOT_CONNECTED                (RASBASE+6)
#define ERROR_EVENT_INVALID                     (RASBASE+7)
#define ERROR_DEVICE_DOES_NOT_EXIST             (RASBASE+8)
#define ERROR_DEVICETYPE_DOES_NOT_EXIST         (RASBASE+9)
#define ERROR_BUFFER_INVALID    		(RASBASE+10)
#define ERROR_ROUTE_NOT_AVAILABLE 		(RASBASE+11)
#define ERROR_ROUTE_NOT_ALLOCATED 		(RASBASE+12)
#define ERROR_INVALID_COMPRESSION_SPECIFIED     (RASBASE+13)
#define ERROR_OUT_OF_BUFFERS    		(RASBASE+14)
#define ERROR_PORT_NOT_FOUND    		(RASBASE+15)
#define ERROR_ASYNC_REQUEST_PENDING 		(RASBASE+16)
#define ERROR_ALREADY_DISCONNECTING 		(RASBASE+17)
#define ERROR_PORT_NOT_OPEN     		(RASBASE+18)
#define ERROR_PORT_DISCONNECTED 		(RASBASE+19)
#define ERROR_NO_ENDPOINTS      		(RASBASE+20)
#define ERROR_CANNOT_OPEN_PHONEBOOK 		(RASBASE+21)
#define ERROR_CANNOT_LOAD_PHONEBOOK 		(RASBASE+22)
#define ERROR_CANNOT_FIND_PHONEBOOK_ENTRY       (RASBASE+23)
#define ERROR_CANNOT_WRITE_PHONEBOOK            (RASBASE+24)
#define ERROR_CORRUPT_PHONEBOOK 		(RASBASE+25)
#define ERROR_CANNOT_LOAD_STRING        	(RASBASE+26)
#define ERROR_KEY_NOT_FOUND     		(RASBASE+27)
#define ERROR_DISCONNECTION     		(RASBASE+28)
#define ERROR_REMOTE_DISCONNECTION      	(RASBASE+29)
#define ERROR_HARDWARE_FAILURE          	(RASBASE+30)
#define ERROR_USER_DISCONNECTION        	(RASBASE+31)
#define ERROR_INVALID_SIZE      		(RASBASE+32)
#define ERROR_PORT_NOT_AVAILABLE 		(RASBASE+33)
#define ERROR_CANNOT_PROJECT_CLIEN      	(RASBASE+34)
#define ERROR_UNKNOWN 				(RASBASE+35)
#define ERROR_WRONG_DEVICE_ATTACHED 		(RASBASE+36)
#define ERROR_BAD_STRING 			(RASBASE+37)
#define ERROR_REQUEST_TIMEOUT 			(RASBASE+38)
#define ERROR_CANNOT_GET_LANA 			(RASBASE+39)
#define ERROR_NETBIOS_ERROR 			(RASBASE+40)
#define ERROR_SERVER_OUT_OF_RESOURCES 		(RASBASE+41)
#define ERROR_NAME_EXISTS_ON_NET 		(RASBASE+42)
#define WARNING_MSG_ALIAS_NOT_ADDED 		(RASBASE+44)
#define ERROR_AUTH_INTERNAL 			(RASBASE+45)
#define ERROR_RESTRICTED_LOGON_HOURS 		(RASBASE+46)
#define ERROR_ACCT_DISABLED 			(RASBASE+47)
#define ERROR_PASSWD_EXPIRED 			(RASBASE+48)
#define ERROR_NO_DIALIN_PERMISSION 		(RASBASE+49)
#define ERROR_SERVER_NOT_RESPONDING 		(RASBASE+50)
#define ERROR_FROM_DEVICE 			(RASBASE+51)
#define ERROR_UNRECOGNIZED_RESPONSE 		(RASBASE+52)
#define ERROR_MACRO_NOT_FOUND 			(RASBASE+53)
#define ERROR_MACRO_NOT_DEFINED 		(RASBASE+54)
#define ERROR_MESSAGE_MACRO_NOT_FOUND 		(RASBASE+55)
#define ERROR_DEFAULTOFF_MACRO_NOT_FOUND 	(RASBASE+56)
#define ERROR_FILE_COULD_NOT_BE_OPENED 		(RASBASE+57)
#define ERROR_DEVICENAME_TOO_LONG 		(RASBASE+58)
#define ERROR_DEVICENAME_NOT_FOUND 		(RASBASE+59)
#define ERROR_NO_RESPONSES 			(RASBASE+60)
#define ERROR_NO_COMMAND_FOUND 			(RASBASE+61)
#define ERROR_WRONG_KEY_SPECIFIED 		(RASBASE+62)
#define ERROR_UNKNOWN_DEVICE_TYPE 		(RASBASE+63)
#define ERROR_ALLOCATING_MEMORY 		(RASBASE+64)
#define ERROR_PORT_NOT_CONFIGURED 		(RASBASE+65)
#define ERROR_DEVICE_NOT_READY 			(RASBASE+66)
#define ERROR_READING_INI_FILE 			(RASBASE+67)
#define ERROR_NO_CONNECTION 			(RASBASE+68)
#define ERROR_BAD_USAGE_IN_INI_FILE 		(RASBASE+69)
#define ERROR_READING_SECTIONNAME 		(RASBASE+70)
#define ERROR_READING_DEVICETYPE 		(RASBASE+71)
#define ERROR_READING_DEVICENAME 		(RASBASE+72)
#define ERROR_READING_USAGE 			(RASBASE+73)
#define ERROR_READING_MAXCONNECTBPS 		(RASBASE+74)
#define ERROR_READING_MAXCARRIERBPS 		(RASBASE+75)
#define ERROR_LINE_BUSY 			(RASBASE+76)
#define ERROR_VOICE_ANSWER 			(RASBASE+77)
#define ERROR_NO_ANSWER 			(RASBASE+78)
#define ERROR_NO_CARRIER 			(RASBASE+79)
#define ERROR_NO_DIALTONE 			(RASBASE+80)
#define ERROR_IN_COMMAND 			(RASBASE+81)
#define ERROR_WRITING_SECTIONNAME 		(RASBASE+82)
#define ERROR_WRITING_DEVICETYPE 		(RASBASE+83)
#define ERROR_WRITING_DEVICENAME 		(RASBASE+84)
#define ERROR_WRITING_MAXCONNECTBPS 		(RASBASE+85)
#define ERROR_WRITING_MAXCARRIERBPS 		(RASBASE+86)
#define ERROR_WRITING_USAGE 			(RASBASE+87)
#define ERROR_WRITING_DEFAULTOFF 		(RASBASE+88)
#define ERROR_READING_DEFAULTOFF 		(RASBASE+89)
#define ERROR_EMPTY_INI_FILE 			(RASBASE+90)
#define ERROR_AUTHENTICATION_FAILURE 		(RASBASE+91)
#define ERROR_PORT_OR_DEVICE 			(RASBASE+92)
#define ERROR_NOT_BINARY_MACRO 			(RASBASE+93)
#define ERROR_DCB_NOT_FOUND 			(RASBASE+94)
#define ERROR_STATE_MACHINES_NOT_STARTED 	(RASBASE+95)
#define ERROR_STATE_MACHINES_ALREADY_STARTED 	(RASBASE+96)
#define ERROR_PARTIAL_RESPONSE_LOOPING 		(RASBASE+97)
#define ERROR_UNKNOWN_RESPONSE_KEY 		(RASBASE+98)
#define ERROR_RECV_BUF_FULL 			(RASBASE+99)
#define ERROR_CMD_TOO_LONG 			(RASBASE+100)
#define ERROR_UNSUPPORTED_BPS 			(RASBASE+101)
#define ERROR_UNEXPECTED_RESPONSE 		(RASBASE+102)
#define ERROR_INTERACTIVE_MODE 			(RASBASE+103)
#define ERROR_BAD_CALLBACK_NUMBER 		(RASBASE+104)
#define ERROR_INVALID_AUTH_STATE 		(RASBASE+105)
#define ERROR_WRITING_INITBPS 			(RASBASE+106)
#define ERROR_X25_DIAGNOSTIC 			(RASBASE+107)
#define ERROR_ACCT_EXPIRED 			(RASBASE+108)
#define ERROR_CHANGING_PASSWORD 		(RASBASE+109)
#define ERROR_OVERRUN 				(RASBASE+110)
#define ERROR_RASMAN_CANNOT_INITIALIZE 		(RASBASE+111)
#define ERROR_BIPLEX_PORT_NOT_AVAILABLE		(RASBASE+112)
#define ERROR_NO_ACTIVE_ISDN_LINES 		(RASBASE+113)
#define ERROR_NO_ISDN_CHANNELS_AVAILABLE	(RASBASE+114)
#define ERROR_TOO_MANY_LINE_ERRORS 		(RASBASE+115)
#define ERROR_IP_CONFIGURATION 			(RASBASE+116)
#define ERROR_NO_IP_ADDRESSES 			(RASBASE+117)
#define ERROR_PPP_TIMEOUT 			(RASBASE+118)
#define ERROR_PPP_REMOTE_TERMINATED 		(RASBASE+119)
#define ERROR_PPP_NO_PROTOCOLS_CONFIGURED 	(RASBASE+120)
#define ERROR_PPP_NO_RESPONSE 			(RASBASE+121)
#define ERROR_PPP_INVALID_PACKET		(RASBASE+122)
#define ERROR_PHONE_NUMBER_TOO_LONG 		(RASBASE+123)
#define ERROR_IPXCP_NO_DIALOUT_CONFIGURED 	(RASBASE+124)
#define ERROR_IPXCP_NO_DIALIN_CONFIGURED 	(RASBASE+125)
#define ERROR_IPXCP_DIALOUT_ALREADY_ACTIVE 	(RASBASE+126)
#define ERROR_ACCESSING_TCPCFGDLL 		(RASBASE+127)
#define ERROR_NO_IP_RAS_ADAPTER 		(RASBASE+128)
#define ERROR_SLIP_REQUIRES_IP 			(RASBASE+129)
#define ERROR_PROJECTION_NOT_COMPLETE 		(RASBASE+130)
#define ERROR_PROTOCOL_NOT_CONFIGURED 		(RASBASE+131)
#define ERROR_PPP_NOT_CONVERGING 		(RASBASE+132)
#define ERROR_PPP_CP_REJECTED 			(RASBASE+133)
#define ERROR_PPP_LCP_TERMINATED 		(RASBASE+134)
#define ERROR_PPP_REQUIRED_ADDRESS_REJECTED 	(RASBASE+135)
#define ERROR_PPP_NCP_TERMINATED 		(RASBASE+136)
#define ERROR_PPP_LOOPBACK_DETECTED 		(RASBASE+137)
#define ERROR_PPP_NO_ADDRESS_ASSIGNED 		(RASBASE+138)
#define ERROR_CANNOT_USE_LOGON_CREDENTIALS 	(RASBASE+139)
#define ERROR_TAPI_CONFIGURATION 		(RASBASE+140)
#define ERROR_NO_LOCAL_ENCRYPTION 		(RASBASE+141)
#define ERROR_NO_REMOTE_ENCRYPTION 		(RASBASE+142)
#define ERROR_REMOTE_REQUIRES_ENCRYPTION 	(RASBASE+143)
#define ERROR_IPXCP_NET_NUMBER_CONFLICT 	(RASBASE+144)
#define ERROR_INVALID_SMM 			(RASBASE+145)
#define ERROR_SMM_UNINITIALIZED 		(RASBASE+146)
#define ERROR_NO_MAC_FOR_PORT 			(RASBASE+147)
#define ERROR_SMM_TIMEOUT 			(RASBASE+148)
#define ERROR_BAD_PHONE_NUMBER 			(RASBASE+149)
#define ERROR_WRONG_MODULE 			(RASBASE+150)
#define ERROR_PPP_MAC 				(RASBASE+151)
#define ERROR_PPP_LCP 				(RASBASE+152)
#define ERROR_PPP_AUTH 				(RASBASE+153)
#define ERROR_PPP_NCP 				(RASBASE+154)
#define ERROR_POWER_OFF 			(RASBASE+155)
#define ERROR_POWER_OFF_CD 			(RASBASE+156)

typedef struct tagRASDEVINFOA {
    DWORD    dwSize;
    CHAR     szDeviceType[ RAS_MaxDeviceType + 1 ];
    CHAR     szDeviceName[ RAS_MaxDeviceName + 1 ];
} RASDEVINFOA, *LPRASDEVINFOA;

typedef struct tagRASDEVINFOW {
    DWORD    dwSize;
    WCHAR    szDeviceType[ RAS_MaxDeviceType + 1 ];
    WCHAR    szDeviceName[ RAS_MaxDeviceName + 1 ];
} RASDEVINFOW, *LPRASDEVINFOW;

DECL_WINELIB_TYPE_AW(RASDEVINFO)
DECL_WINELIB_TYPE_AW(LPRASDEVINFO)

DECLARE_HANDLE(HRASCONN);
typedef  HRASCONN* LPHRASCONN;

typedef struct tagRASCONNA {
    DWORD    dwSize;
    HRASCONN hRasConn;
    CHAR     szEntryName[ RAS_MaxEntryName + 1 ];
    CHAR     szDeviceType[ RAS_MaxDeviceType + 1 ];
    CHAR     szDeviceName[ RAS_MaxDeviceName + 1 ];
    CHAR     szPhonebook[ MAX_PATH ];
    DWORD    dwSubEntry;
    GUID     guidEntry;
    DWORD    dwFlags;
    LUID     luid;
    GUID     guidCorrelationId;
} RASCONNA,*LPRASCONNA;

typedef struct tagRASCONNW {
    DWORD    dwSize;
    HRASCONN hRasConn;
    WCHAR    szEntryName[ RAS_MaxEntryName + 1 ];
    WCHAR    szDeviceType[ RAS_MaxDeviceType + 1 ];
    WCHAR    szDeviceName[ RAS_MaxDeviceName + 1 ];
    WCHAR    szPhonebook[ MAX_PATH ];
    DWORD    dwSubEntry;
    GUID     guidEntry;
    DWORD    dwFlags;
    LUID     luid;
    GUID     guidCorrelationId;
} RASCONNW,*LPRASCONNW;

DECL_WINELIB_TYPE_AW(RASCONN)
DECL_WINELIB_TYPE_AW(LPRASCONN)

typedef struct tagRASENTRYNAMEA {
    DWORD dwSize;
    CHAR  szEntryName[ RAS_MaxEntryName + 1 ];
} RASENTRYNAMEA, *LPRASENTRYNAMEA;

typedef struct tagRASENTRYNAMEW {
    DWORD dwSize;
    WCHAR szEntryName[ RAS_MaxEntryName + 1 ];
} RASENTRYNAMEW, *LPRASENTRYNAMEW;

DECL_WINELIB_TYPE_AW(RASENTRYNAME)
DECL_WINELIB_TYPE_AW(LPRASENTRYNAME)

typedef struct tagRASDIALPARAMSA {
    DWORD dwSize;
    CHAR szEntryName[ RAS_MaxEntryName + 1 ];
    CHAR szPhoneNumber[ RAS_MaxPhoneNumber + 1 ];
    CHAR szCallbackNumber[ RAS_MaxCallbackNumber + 1 ];
    CHAR szUserName[ UNLEN + 1 ];
    CHAR szPassword[ PWLEN + 1 ];
    CHAR szDomain[ DNLEN + 1 ];
    DWORD dwSubEntry;
    DWORD dwCallbackId;
} RASDIALPARAMSA, *LPRASDIALPARAMSA;

typedef struct tagRASDIALPARAMSW {
    DWORD dwSize;
    WCHAR szEntryName[ RAS_MaxEntryName + 1 ];
    WCHAR szPhoneNumber[ RAS_MaxPhoneNumber + 1 ];
    WCHAR szCallbackNumber[ RAS_MaxCallbackNumber + 1 ];
    WCHAR szUserName[ UNLEN + 1 ];
    WCHAR szPassword[ PWLEN + 1 ];
    WCHAR szDomain[ DNLEN + 1 ];
    DWORD dwSubEntry;
    DWORD dwCallbackId;
} RASDIALPARAMSW, *LPRASDIALPARAMSW;

DECL_WINELIB_TYPE_AW(RASDIALPARAMS)
DECL_WINELIB_TYPE_AW(LPRASDIALPARAMS)

typedef struct tagRASIPADDR {
	BYTE classA,classB,classC,classD;
} RASIPADDR;

#define RASEO_UseCountryAndAreaCodes	0x0001
#define RASEO_SpecificIpAddr		0x0002
#define RASEO_SpecificNameServers	0x0004
#define RASEO_IpHeaderCompression	0x0008
#define RASEO_RemoteDefaultGateway	0x0010
#define RASEO_DisableLcpExtensions	0x0020
#define RASEO_TerminalBeforeDial	0x0040
#define RASEO_TerminalAfterDial		0x0080
#define RASEO_ModemLights		0x0100
#define RASEO_SwCompression		0x0200
#define RASEO_RequireEncryptedPw	0x0400
#define RASEO_RequireMsEncryptedPw	0x0800
#define RASEO_RequireDataEncryption	0x1000
#define RASEO_NetworkLogon		0x2000
#define RASEO_UseLogonCredentials	0x4000
#define RASEO_PromoteAlternates		0x8000
typedef struct tagRASENTRYA {
    DWORD dwSize;
    DWORD dwfOptions;

    /* Location */

    DWORD dwCountryID;
    DWORD dwCountryCode;
    CHAR szAreaCode[ RAS_MaxAreaCode + 1 ];
    CHAR szLocalPhoneNumber[ RAS_MaxPhoneNumber + 1 ];
    DWORD dwAlternateOffset;

    /* IP related stuff */

    RASIPADDR ipaddr;
    RASIPADDR ipaddrDns;
    RASIPADDR ipaddrDnsAlt;
    RASIPADDR ipaddrWins;
    RASIPADDR ipaddrWinsAlt;

    /* Framing (for ppp/isdn etc...) */

    DWORD dwFrameSize;
    DWORD dwfNetProtocols;
    DWORD dwFramingProtocol;

    CHAR szScript[ MAX_PATH ];

    CHAR szAutodialDll[ MAX_PATH ];
    CHAR szAutodialFunc[ MAX_PATH ];

    CHAR szDeviceType[ RAS_MaxDeviceType + 1 ];
    CHAR szDeviceName[ RAS_MaxDeviceName + 1 ];

    /* x25 only */

    CHAR szX25PadType[ RAS_MaxPadType + 1 ];
    CHAR szX25Address[ RAS_MaxX25Address + 1 ];
    CHAR szX25Facilities[ RAS_MaxFacilities + 1 ];
    CHAR szX25UserData[ RAS_MaxUserData + 1 ];
    DWORD dwChannels;

    DWORD dwReserved1;
    DWORD dwReserved2;

    /* Multilink and BAP */

    DWORD dwSubEntries;
    DWORD dwDialMode;
    DWORD dwDialExtraPercent;
    DWORD dwDialExtraSampleSeconds;
    DWORD dwHangUpExtraPercent;
    DWORD dwHangUpExtraSampleSeconds;

    /* Idle time out */
    DWORD dwIdleDisconnectSeconds;

    DWORD dwType;		/* entry type */
    DWORD dwEncryptionType;	/* type of encryption to use */
    DWORD dwCustomAuthKey;	/* authentication key for EAP */
    GUID guidId;		/* guid that represents the phone-book entry  */
    CHAR szCustomDialDll[MAX_PATH];    /* DLL for custom dialing  */
    DWORD dwVpnStrategy;         /* specifies type of VPN protocol */

    DWORD dwfOptions2;
    DWORD dwfOptions3;
    CHAR szDnsSuffix[RAS_MaxDnsSuffix];
    DWORD dwTcpWindowSize;
    CHAR szPrerequisitePbk[MAX_PATH];
    CHAR szPrerequisiteEntry[RAS_MaxEntryName + 1];
    DWORD dwRedialCount;
    DWORD dwRedialPause;
} RASENTRYA, *LPRASENTRYA;

typedef struct tagRASENTRYW {
    DWORD dwSize;
    DWORD dwfOptions;

    /* Location */

    DWORD dwCountryID;
    DWORD dwCountryCode;
    WCHAR szAreaCode[ RAS_MaxAreaCode + 1 ];
    WCHAR szLocalPhoneNumber[ RAS_MaxPhoneNumber + 1 ];
    DWORD dwAlternateOffset;

    /* IP related stuff */

    RASIPADDR ipaddr;
    RASIPADDR ipaddrDns;
    RASIPADDR ipaddrDnsAlt;
    RASIPADDR ipaddrWins;
    RASIPADDR ipaddrWinsAlt;

    /* Framing (for ppp/isdn etc...) */

    DWORD dwFrameSize;
    DWORD dwfNetProtocols;
    DWORD dwFramingProtocol;

    WCHAR szScript[ MAX_PATH ];

    WCHAR szAutodialDll[ MAX_PATH ];
    WCHAR szAutodialFunc[ MAX_PATH ];

    WCHAR szDeviceType[ RAS_MaxDeviceType + 1 ];
    WCHAR szDeviceName[ RAS_MaxDeviceName + 1 ];

    /* x25 only */

    WCHAR szX25PadType[ RAS_MaxPadType + 1 ];
    WCHAR szX25Address[ RAS_MaxX25Address + 1 ];
    WCHAR szX25Facilities[ RAS_MaxFacilities + 1 ];
    WCHAR szX25UserData[ RAS_MaxUserData + 1 ];
    DWORD dwChannels;

    DWORD dwReserved1;
    DWORD dwReserved2;

    /* Multilink and BAP */

    DWORD dwSubEntries;
    DWORD dwDialMode;
    DWORD dwDialExtraPercent;
    DWORD dwDialExtraSampleSeconds;
    DWORD dwHangUpExtraPercent;
    DWORD dwHangUpExtraSampleSeconds;

    /* Idle time out */
    DWORD dwIdleDisconnectSeconds;

    DWORD dwType;		/* entry type */
    DWORD dwEncryptionType;	/* type of encryption to use */
    DWORD dwCustomAuthKey;	/* authentication key for EAP */
    GUID guidId;		/* guid that represents the phone-book entry  */
    WCHAR szCustomDialDll[MAX_PATH];    /* DLL for custom dialing  */
    DWORD dwVpnStrategy;         /* specifies type of VPN protocol */

    DWORD dwfOptions2;
    DWORD dwfOptions3;
    WCHAR szDnsSuffix[RAS_MaxDnsSuffix];
    DWORD dwTcpWindowSize;
    WCHAR szPrerequisitePbk[MAX_PATH];
    WCHAR szPrerequisiteEntry[RAS_MaxEntryName + 1];
    DWORD dwRedialCount;
    DWORD dwRedialPause;
} RASENTRYW, *LPRASENTRYW;

DECL_WINELIB_TYPE_AW(RASENTRY)

#define RASCS_PAUSED 0x1000
#define RASCS_DONE   0x2000
typedef enum tagRASCONNSTATE
{
      RASCS_OpenPort = 0,
      RASCS_PortOpened,
      RASCS_ConnectDevice,
      RASCS_DeviceConnected,
      RASCS_AllDevicesConnected,
      RASCS_Authenticate,
      RASCS_AuthNotify,
      RASCS_AuthRetry,
      RASCS_AuthCallback,
      RASCS_AuthChangePassword,
      RASCS_AuthProject,
      RASCS_AuthLinkSpeed,
      RASCS_AuthAck,
      RASCS_ReAuthenticate,
      RASCS_Authenticated,
      RASCS_PrepareForCallback,
      RASCS_WaitForModemReset,
      RASCS_WaitForCallback,
      RASCS_Projected,
      RASCS_StartAuthentication,
      RASCS_CallbackComplete,
      RASCS_LogonNetwork,
      RASCS_SubEntryConnected,
      RASCS_SubEntryDisconnected,
      RASCS_Interactive = RASCS_PAUSED,
      RASCS_RetryAuthentication,
      RASCS_CallbackSetByCaller,
      RASCS_PasswordExpired,
      RASCS_Connected = RASCS_DONE,
      RASCS_Disconnected
}  RASCONNSTATE, *LPRASCONNSTATE;

typedef struct tagRASCONNSTATUSA
{
    DWORD dwSize;
    RASCONNSTATE rasconnstate;
    DWORD dwError;
    CHAR szDeviceType[RAS_MaxDeviceType + 1];
    CHAR szDeviceName[RAS_MaxDeviceName + 1];
} RASCONNSTATUSA, *LPRASCONNSTATUSA;

typedef struct tagRASCONNSTATUSW
{
    DWORD dwSize;
    RASCONNSTATE rasconnstate;
    DWORD dwError;
    WCHAR szDeviceType[RAS_MaxDeviceType + 1];
    WCHAR szDeviceName[RAS_MaxDeviceName + 1];
} RASCONNSTATUSW, *LPRASCONNSTATUSW;

DECL_WINELIB_TYPE_AW(RASCONNSTATUS)

typedef enum tagRASPROJECTION
{
    RASP_Amb =    0x10000,
    RASP_PppNbf = 0x803F,
    RASP_PppIpx = 0x802B,
    RASP_PppIp =  0x8021,
    RASP_PppLcp = 0xC021,
    RASP_Slip =   0x20000
} RASPROJECTION, *LPRASPROJECTION;

typedef struct tagRASSUBENTRYA
{
    DWORD dwSize;
    DWORD dwfFlags;
    CHAR szDeviceType[RAS_MaxDeviceType + 1];
    CHAR szDeviceName[RAS_MaxDeviceName + 1];
    CHAR szLocalPhoneNumber[RAS_MaxPhoneNumber + 1];
    DWORD dwAlternateOffset;
} RASSUBENTRYA, *LPRASSUBENTRYA;

typedef struct tagRASSUBENTRYW
{
    DWORD dwSize;
    DWORD dwfFlags;
    WCHAR szDeviceType[RAS_MaxDeviceType + 1];
    WCHAR szDeviceName[RAS_MaxDeviceName + 1];
    WCHAR szLocalPhoneNumber[RAS_MaxPhoneNumber + 1];
    DWORD dwAlternateOffset;
} RASSUBENTRYW, *LPRASSUBENTRYW;

typedef struct tagRASDIALEXTENSIONS
{
    DWORD dwSize;
    DWORD dwfOptions;
    HWND hwndParent;
    ULONG_PTR reserved;
} RASDIALEXTENSIONS, *LPRASDIALEXTENSIONS;

typedef struct tagRASAUTODIALENTRYA
{
    DWORD dwSize;
    DWORD dwFlags;
    DWORD dwDialingLocation;
    CHAR  szEntry[ RAS_MaxEntryName + 1 ];
} RASAUTODIALENTRYA, *LPRASAUTODIALENTRYA;

typedef struct tagRASAUTODIALENTRYW
{
    DWORD dwSize;
    DWORD dwFlags;
    DWORD dwDialingLocation;
    WCHAR szEntry[ RAS_MaxEntryName + 1 ];
} RASAUTODIALENTRYW, *LPRASAUTODIALENTRYW;


DWORD WINAPI RasConnectionNotificationA(HRASCONN,HANDLE,DWORD);
DWORD WINAPI RasConnectionNotificationW(HRASCONN,HANDLE,DWORD);
#define      RasConnectionNotification WINELIB_NAME_AW(RasConnectionNotification)
DWORD WINAPI RasCreatePhonebookEntryA(HWND,LPCSTR);
DWORD WINAPI RasCreatePhonebookEntryW(HWND,LPCWSTR);
#define      RasCreatePhonebookEntry WINELIB_NAME_AW(RasCreatePhonebookEntry)
DWORD WINAPI RasDeleteEntryA(LPCSTR,LPCSTR);
DWORD WINAPI RasDeleteEntryW(LPCWSTR,LPCWSTR);
#define      RasDeleteEntry WINELIB_NAME_AW(RasDeleteEntry)
DWORD WINAPI RasDeleteSubEntryA(LPCSTR,LPCSTR,DWORD);
DWORD WINAPI RasDeleteSubEntryW(LPCWSTR,LPCWSTR,DWORD);
#define      RasDeleteSubEntry WINELIB_NAME_AW(RasDeleteSubEntry)
DWORD WINAPI RasDialA(LPRASDIALEXTENSIONS,LPCSTR,LPRASDIALPARAMSA,DWORD,LPVOID,LPHRASCONN);
DWORD WINAPI RasDialW(LPRASDIALEXTENSIONS,LPCWSTR,LPRASDIALPARAMSW,DWORD,LPVOID,LPHRASCONN);
#define      RasDial WINELIB_NAME_AW(RasDial)
DWORD WINAPI RasEditPhonebookEntryA(HWND,LPCSTR,LPCSTR);
DWORD WINAPI RasEditPhonebookEntryW(HWND,LPCWSTR,LPCWSTR);
#define      RasEditPhonebookEntry WINELIB_NAME_AW(RasEditPhonebookEntry)
DWORD WINAPI RasEnumAutodialAddressesA(LPSTR*,LPDWORD,LPDWORD);
DWORD WINAPI RasEnumAutodialAddressesW(LPWSTR*,LPDWORD,LPDWORD);
#define      RasEnumAutodialAddresses WINELIB_NAME_AW(RasEnumAutodialAddresses)
DWORD WINAPI RasEnumConnectionsA(LPRASCONNA,LPDWORD,LPDWORD);
DWORD WINAPI RasEnumConnectionsW(LPRASCONNW,LPDWORD,LPDWORD);
#define      RasEnumConnections WINELIB_NAME_AW(RasEnumConnections)
DWORD WINAPI RasEnumDevicesA(LPRASDEVINFOA,LPDWORD,LPDWORD);
DWORD WINAPI RasEnumDevicesW(LPRASDEVINFOW,LPDWORD,LPDWORD);
#define      RasEnumDevices WINELIB_NAME_AW(RasEnumDevices)
DWORD WINAPI RasEnumEntriesA(LPCSTR,LPCSTR,LPRASENTRYNAMEA,LPDWORD,LPDWORD);
DWORD WINAPI RasEnumEntriesW(LPCWSTR,LPCWSTR,LPRASENTRYNAMEW,LPDWORD,LPDWORD);
#define      RasEnumEntries WINELIB_NAME_AW(RasEnumEntries)
DWORD WINAPI RasGetAutodialAddressA(LPCSTR,LPDWORD,LPRASAUTODIALENTRYA,LPDWORD,LPDWORD);
DWORD WINAPI RasGetAutodialAddressW(LPCWSTR,LPDWORD,LPRASAUTODIALENTRYW,LPDWORD,LPDWORD);
#define      RasGetAutodialAddresses WINELIB_NAME_AW(RasGetAutodialAddresses)
DWORD WINAPI RasGetAutodialEnableA(DWORD,LPBOOL);
DWORD WINAPI RasGetAutodialEnableW(DWORD,LPBOOL);
#define      RasGetAutodialEnable WINELIB_NAME_AW(RasGetAutodialEnable)
DWORD WINAPI RasGetAutodialParamA(DWORD dwKey, LPVOID lpvValue, LPDWORD lpdwcbValue);
DWORD WINAPI RasGetAutodialParamW(DWORD dwKey, LPVOID lpvValue, LPDWORD lpdwcbValue);
#define RasGetAutodialParam WINELIB_NAME_AW(RasGetAutodialParam)
DWORD WINAPI RasGetConnectStatusA(HRASCONN,LPRASCONNSTATUSA);
DWORD WINAPI RasGetConnectStatusW(HRASCONN,LPRASCONNSTATUSW);
#define      RasGetConnectStatus WINELIB_NAME_AW(RasGetConnectStatus)
DWORD WINAPI RasGetEntryDialParamsA(LPCSTR,LPRASDIALPARAMSA,LPBOOL);
DWORD WINAPI RasGetEntryDialParamsW(LPCWSTR,LPRASDIALPARAMSW,LPBOOL);
#define      RasGetEntryDialParams WINELIB_NAME_AW(RasGetEntryDialParams)
DWORD WINAPI RasGetEntryPropertiesA(LPCSTR,LPCSTR,LPRASENTRYA,LPDWORD,LPBYTE,LPDWORD);
DWORD WINAPI RasGetEntryPropertiesW(LPCWSTR,LPCWSTR,LPRASENTRYW,LPDWORD,LPBYTE,LPDWORD);
#define      RasGetEntryProperties WINELIB_NAME_AW(RasGetEntryProperties)
DWORD WINAPI RasGetErrorStringA(UINT,LPSTR,DWORD);
DWORD WINAPI RasGetErrorStringW(UINT,LPWSTR,DWORD);
#define      RasGetErrorString WINELIB_NAME_AW(RasGetErrorString)
DWORD WINAPI RasGetProjectionInfoA(HRASCONN,RASPROJECTION,LPVOID,LPDWORD);
DWORD WINAPI RasGetProjectionInfoW(HRASCONN,RASPROJECTION,LPVOID,LPDWORD);
#define      RasGetProjectionInfo WINELIB_NAME_AW(RasGetProjectionInfo)
DWORD WINAPI RasHangUpA(HRASCONN);
DWORD WINAPI RasHangUpW(HRASCONN);
#define      RasHangUp WINELIB_NAME_AW(RasHangUp)
DWORD WINAPI RasRenameEntryA(LPCSTR,LPCSTR,LPCSTR);
DWORD WINAPI RasRenameEntryW(LPCWSTR,LPCWSTR,LPCWSTR);
#define      RasRenameEntry WINELIB_NAME_AW(RasRenameEntry)
DWORD WINAPI RasSetAutodialAddressA(LPCSTR,DWORD,LPRASAUTODIALENTRYA,DWORD,DWORD);
DWORD WINAPI RasSetAutodialAddressW(LPCWSTR,DWORD,LPRASAUTODIALENTRYW,DWORD,DWORD);
#define      RasSetAutodialAddress WINELIB_NAME_AW(RasSetAutodialAddress)
DWORD WINAPI RasSetAutodialParamA(DWORD,LPVOID,DWORD);
DWORD WINAPI RasSetAutodialParamW(DWORD,LPVOID,DWORD);
#define      RasSetAutodialParam WINELIB_NAME_AW(RasSetAutodialParam)
DWORD WINAPI RasSetEntryDialParamsA(LPCSTR,LPRASDIALPARAMSA,BOOL);
DWORD WINAPI RasSetEntryDialParamsW(LPCWSTR,LPRASDIALPARAMSW,BOOL);
#define      RasSetEntryDialParams WINELIB_NAME_AW(RasSetEntryDialParams)
DWORD WINAPI RasSetSubEntryPropertiesA(LPCSTR,LPCSTR,DWORD,LPRASSUBENTRYA,DWORD,LPBYTE,DWORD);
DWORD WINAPI RasSetSubEntryPropertiesW(LPCWSTR,LPCWSTR,DWORD,LPRASSUBENTRYW,DWORD,LPBYTE,DWORD);
#define      RasSetSubEntryProperties WINELIB_NAME_AW(RasSetSubEntryProperties)
DWORD WINAPI RasValidateEntryNameA(LPCSTR  lpszPhonebook, LPCSTR  lpszEntry);
DWORD WINAPI RasValidateEntryNameW(LPCWSTR lpszPhonebook, LPCWSTR lpszEntry);
#define RasValidateEntryName WINELIB_NAME_AW(RasValidateEntryName)
DWORD WINAPI RasSetEntryPropertiesA(LPCSTR,LPCSTR,LPRASENTRYA,DWORD,LPBYTE,DWORD);
DWORD WINAPI RasSetEntryPropertiesW(LPCWSTR,LPCWSTR,LPRASENTRYW,DWORD,LPBYTE,DWORD);
#define RasSetEntryProperties WINELIB_NAME_AW(RasSetEntryProperties)
DWORD WINAPI RasSetAutodialEnableA(DWORD dwDialingLocation, BOOL fEnabled);
DWORD WINAPI RasSetAutodialEnableW(DWORD dwDialingLocation, BOOL fEnabled);
#define RasSetAutodialEnable WINELIB_NAME_AW(RasSetAutodialEnable)

#include <poppack.h>
#ifdef __cplusplus
}
#endif

#endif
