/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include "wmpids.h"
#include <shellapi.h>

BOOL is_full_path(BSTR path);
int inner_lstrncmpW(WCHAR* str1, WCHAR* str2, int len);

/**********************************************************
 * IWMPControls methods implementation
 */

#define CONTROLS_THIS(iface) DEFINE_THIS(Player, WMPControls, iface)

static HRESULT WINAPI WMPControls_QueryInterface(
        IWMPControls* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = CONTROLS_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI WMPControls_AddRef(IWMPControls* iface)
{
    Player *This = CONTROLS_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI WMPControls_Release(IWMPControls* iface)
{
    Player *This = CONTROLS_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI WMPControls_GetTypeInfoCount(
        IWMPControls* iface,
        UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPControls_GetTypeInfo(
        IWMPControls* iface,
        UINT iTInfo,
        LCID lcid,
        ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPControls_GetIDsOfNames(
        IWMPControls* iface,
        REFIID riid,
        LPOLESTR *rgszNames,
        UINT cNames,
        LCID lcid,
        DISPID *rgDispId)
{
    Player *This = CONTROLS_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}


static HRESULT WINAPI WMPControls_Invoke(
        IWMPControls* iface,
        DISPID dispIdMember,
        REFIID riid,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pDispParams,
        VARIANT *pVarResult,
        EXCEPINFO *pExcepInfo,
        UINT *puArgErr)
{
    Player *This = CONTROLS_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPControls methods ***/
static HRESULT WINAPI WMPControls_get_isAvailable(
        IWMPControls* iface,
        BSTR bstrItem,
        VARIANT_BOOL *pIsAvailable)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_play(IWMPControls* iface)
{
    Player *This = CONTROLS_THIS(iface);
    HRESULT hrResult;
    WCHAR command[MAX_PATH] = {'c',':','\\','w','i','n','d','o','w','s','\\',
        'c','o','m','m','a','n','d','\\','m','p','l','a','y','e','r',' ',
        'c',':','\\','w','i','n','d','o','w','s','\\','t','e','m','p','\\',
        'w','m','p','.','t','m','p',0};
    WCHAR file_path[] = {'c',':','\\','w','i','n','d','o','w','s','\\','t','e','m','p','\\',
        'w','m','p','.','t','m','p',0};
    WCHAR url[MAX_PATH];

    url[0] = '\0';
    if(!is_full_path(This->url))
    {
        lstrcpyW(url, This->baseurl);
    }
    lstrcatW(url, This->url);
    hrResult = URLDownloadToFileW(NULL,url,file_path,0,NULL);
    if(FAILED(hrResult)) {
        ERR("Cannot download media file\n");
        return E_FAIL;
    }
    ShellExecuteW(0,0, command, 0, 0, 0);
    TRACE("Play %s\n", debugstr_w(url));
    return S_OK;
}


static HRESULT WINAPI WMPControls_stop(IWMPControls* iface)
{
    TRACE("returns S_OK\n");
    return S_OK;
}


static HRESULT WINAPI WMPControls_pause(IWMPControls* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_fastForward(IWMPControls* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_fastReverse(IWMPControls* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_get_currentPosition(
        IWMPControls* iface,
        double *pdCurrentPosition)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_put_currentPosition(
        IWMPControls* iface,
        double dCurrentPosition)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_get_currentPositionString(
        IWMPControls* iface,
        BSTR *pbstrCurrentPosition)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_next(IWMPControls* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_previous(IWMPControls* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_get_currentItem(
        IWMPControls* iface,
        IWMPMedia **ppIWMPMedia)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_put_currentItem(
        IWMPControls* iface,
        IWMPMedia *pIWMPMedia)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_get_currentMarker(
        IWMPControls* iface,
        long *plMarker)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_put_currentMarker(
        IWMPControls* iface,
        long lMarker)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPControls_playItem(
        IWMPControls* iface,
        IWMPMedia *pIWMPMedia)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static const IWMPControlsVtbl WMPControlsVtbl =
{
    WMPControls_QueryInterface,
    WMPControls_AddRef,
    WMPControls_Release,
    WMPControls_GetTypeInfoCount,
    WMPControls_GetTypeInfo,
    WMPControls_GetIDsOfNames,
    WMPControls_Invoke,
    WMPControls_get_isAvailable,
    WMPControls_play,
    WMPControls_stop,
    WMPControls_pause,
    WMPControls_fastForward,
    WMPControls_fastReverse,
    WMPControls_get_currentPosition,
    WMPControls_put_currentPosition,
    WMPControls_get_currentPositionString,
    WMPControls_next,
    WMPControls_previous,
    WMPControls_get_currentItem,
    WMPControls_put_currentItem,
    /* FIXME:  get_currentMarker and put_currentMarker - not in MSDN */
    WMPControls_get_currentMarker,
    WMPControls_put_currentMarker,
    WMPControls_playItem
};

#undef CONTROLS_THIS

void Player_WMPControls_Init(Player *This)
{
    This->lpWMPControlsVtbl = &WMPControlsVtbl;
}

int inner_lstrncmpW(WCHAR* str1, WCHAR* str2, int len)
{
    int i;
    if ((str1 == NULL) && (str2 == NULL)) return 0;
    if (str1 == NULL) return -1;
    if (str2 == NULL) return 1;
    for (i = 0; i < len; i++)
    {
        if(str1[i] > str2[i]) return 1;
        if(str1[i] < str2[i]) return -1;
    }
    return 0;
}

BOOL is_full_path(BSTR path)
{
    WCHAR str_ftp[] = {'f','t','p',':','/','/',0};
    WCHAR str_http[] = {'h','t','t','p',':','/','/',0};
    return (!inner_lstrncmpW(path, str_ftp, lstrlenW(str_ftp)) || !inner_lstrncmpW(path, str_http, lstrlenW(str_http)));
}
