/*
 * IMediaPlayer interface functions
 *
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include <shellapi.h>

/**********************************************************
 * IMediaPlayer methods implementation
 */

#define MEDIAPLAYER_THIS(iface) DEFINE_THIS(Player, MediaPlayer, iface)

static HRESULT WINAPI MediaPlayer_QueryInterface(
        IMediaPlayer* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI MediaPlayer_AddRef(
        IMediaPlayer* iface)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI MediaPlayer_Release(
        IMediaPlayer* iface)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI MediaPlayer_GetTypeInfoCount(
             IMediaPlayer* iface,
             UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI MediaPlayer_GetTypeInfo(
             IMediaPlayer* iface,
             UINT iTInfo,
             LCID lcid,
             ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI MediaPlayer_GetIDsOfNames(
             IMediaPlayer* iface,
             REFIID riid,
             LPOLESTR *rgszNames,
             UINT cNames,
             LCID lcid,
             DISPID *rgDispId)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI MediaPlayer_Invoke(
             IMediaPlayer* iface,
             DISPID dispIdMember,
             REFIID riid,
             LCID lcid,
             WORD wFlags,
             DISPPARAMS *pDispParams,
             VARIANT *pVarResult,
             EXCEPINFO *pExcepInfo,
             UINT *puArgErr)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

    /*** IMediaPlayer methods ***/
static double WINAPI MediaPlayer_get_CurrentPosition(
            IMediaPlayer* iface)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    TRACE("%lf\n", This->CurrentPosition);
    return This->CurrentPosition;
}

static void WINAPI MediaPlayer_put_CurrentPosition(
          IMediaPlayer* iface,
          double pCurrentPosition)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    TRACE("%lf\n", pCurrentPosition);
    This->CurrentPosition = pCurrentPosition;
}

static double WINAPI MediaPlayer_get_Duration(
            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ImageSourceWidth(
        IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ImageSourceHeight(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_MarkerCount(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_CanScan(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_CanSeek(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_CanSeekToMarkers(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_CurrentMarker(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_CurrentMarker(
          IMediaPlayer* iface,
          long pCurrentMarker)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_FileName(
          IMediaPlayer* iface)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    TRACE("\n");
    return This->FileName;
}

static void WINAPI MediaPlayer_put_FileName(
          IMediaPlayer* iface,
          BSTR pbstrFileName)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    TRACE("%s\n", debugstr_w(pbstrFileName));
    if(pbstrFileName)
        lstrcpyW(This->FileName, pbstrFileName);
}

static BSTR WINAPI MediaPlayer_get_SourceLink(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static DATE WINAPI MediaPlayer_get_CreationDate(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ErrorCorrection(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_Bandwidth(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_SourceProtocol(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ReceivedPackets(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_RecoveredPackets(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_LostPackets(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ReceptionQuality(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_BufferingCount(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_IsBroadcast(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_BufferingProgress(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ChannelName(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ChannelDescription(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ChannelURL(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ContactAddress(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ContactPhone(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ContactEmail(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static double WINAPI MediaPlayer_get_BufferingTime(
            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_BufferingTime(
          IMediaPlayer* iface,
          double pBufferingTime)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AutoStart(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AutoStart(
          IMediaPlayer* iface,
          VARIANT_BOOL pAutoStart)
{
    if(pAutoStart)
        FIXME("true\n");
    else
        FIXME("false\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AutoRewind(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AutoRewind(
          IMediaPlayer* iface,
          VARIANT_BOOL pAutoRewind)
{
    FIXME("is not implemented\n");
}

static double WINAPI MediaPlayer_get_Rate(
            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_Rate(
          IMediaPlayer* iface,
          double pRate)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendKeyboardEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendKeyboardEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendKeyboardEvents)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendMouseClickEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendMouseClickEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendMouseClickEvents)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendMouseMoveEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendMouseMoveEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendMouseMoveEvents)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_PlayCount(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_PlayCount(
          IMediaPlayer* iface,
          long pPlayCount)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ClickToPlay(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ClickToPlay(
          IMediaPlayer* iface,
          VARIANT_BOOL pClickToPlay)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AllowScan(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AllowScan(
          IMediaPlayer* iface,
          VARIANT_BOOL pAllowScan)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_EnableContextMenu(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_EnableContextMenu(
          IMediaPlayer* iface,
          VARIANT_BOOL pEnableContextMenu)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_CursorType(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_CursorType(
          IMediaPlayer* iface,
          long pCursorType)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_CodecCount(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AllowChangeDisplaySize(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AllowChangeDisplaySize(
          IMediaPlayer* iface,
          VARIANT_BOOL pAllowChangeDisplaySize)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_IsDurationValid(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_OpenState(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendOpenStateChangeEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendOpenStateChangeEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendOpenStateChangeEvents)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendWarningEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendWarningEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendWarningEvents)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendErrorEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendErrorEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendErrorEvents)
{
    FIXME("is not implemented\n");
}

static MPPlayStateConstants WINAPI MediaPlayer_get_PlayState(
                          IMediaPlayer* iface)
{
    FIXME("stub, return mpStopped\n");
    return mpStopped;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_SendPlayStateChangeEvents(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SendPlayStateChangeEvents(
          IMediaPlayer* iface,
          VARIANT_BOOL pSendPlayStateChangeEvents)
{
    FIXME("is not implemented\n");
}

static MPDisplaySizeConstants WINAPI MediaPlayer_get_DisplaySize(
                            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_DisplaySize(
          IMediaPlayer* iface,
          MPDisplaySizeConstants pDisplaySize)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_InvokeURLs(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_InvokeURLs(
          IMediaPlayer* iface,
          VARIANT_BOOL pInvokeURLs)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_BaseURL(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_BaseURL(
          IMediaPlayer* iface,
          BSTR pbstrBaseURL)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_DefaultFrame(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_DefaultFrame(
          IMediaPlayer* iface,
          BSTR pbstrDefaultFrame)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_HasError(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ErrorDescription(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ErrorCode(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AnimationAtStart(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AnimationAtStart(
          IMediaPlayer* iface,
          VARIANT_BOOL pAnimationAtStart)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_TransparentAtStart(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_TransparentAtStart(
          IMediaPlayer* iface,
          VARIANT_BOOL pTransparentAtStart)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_Volume(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_Volume(
          IMediaPlayer* iface,
          long pVolume)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_Balance(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_Balance(
          IMediaPlayer* iface,
          long pBalance)
{
    FIXME("is not implemented\n");
}

static MPReadyStateConstants WINAPI MediaPlayer_get_ReadyState(
                           IMediaPlayer* iface)
{
    FIXME("stub, return mpReadyStateComplete\n");
    return mpReadyStateComplete;
}

static double WINAPI MediaPlayer_get_SelectionStart(
            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SelectionStart(
          IMediaPlayer* iface,
          double pValue)
{
    FIXME("is not implemented\n");
}

static double WINAPI MediaPlayer_get_SelectionEnd(
            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SelectionEnd(
          IMediaPlayer* iface,
          double pValue)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowDisplay(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowDisplay(
          IMediaPlayer* iface,
          VARIANT_BOOL Show)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowControls(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowControls(
          IMediaPlayer* iface,
          VARIANT_BOOL Show)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowPositionControls(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowPositionControls(
          IMediaPlayer* iface,
          VARIANT_BOOL Show)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowTracker(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowTracker(
          IMediaPlayer* iface,
          VARIANT_BOOL Show)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_EnablePositionControls(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_EnablePositionControls(
          IMediaPlayer* iface,
          VARIANT_BOOL Enable)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_EnableTracker(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_EnableTracker(
          IMediaPlayer* iface,
          VARIANT_BOOL Enable)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_Enabled(
                  IMediaPlayer* iface)
{
    FIXME("stub, return VARIANT_TRUE\n");
    return VARIANT_TRUE;
}

static void WINAPI MediaPlayer_put_Enabled(
          IMediaPlayer* iface,
          VARIANT_BOOL pEnabled)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_DisplayForeColor(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_DisplayForeColor(
          IMediaPlayer* iface,
          long ForeColor)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_DisplayBackColor(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_DisplayBackColor(
          IMediaPlayer* iface,
          long BackColor)
{
    FIXME("is not implemented\n");
}

static MPDisplayModeConstants WINAPI MediaPlayer_get_DisplayMode(
                            IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_DisplayMode(
          IMediaPlayer* iface,
          MPDisplayModeConstants pValue)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_VideoBorder3D(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_VideoBorder3D(
          IMediaPlayer* iface,
          VARIANT_BOOL pVideoBorderWidth)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_VideoBorderWidth(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_VideoBorderWidth(
          IMediaPlayer* iface,
          long pVideoBorderWidth)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_VideoBorderColor(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_VideoBorderColor(
          IMediaPlayer* iface,
          long pVideoBorderWidth)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowGotoBar(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowGotoBar(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowStatusBar(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowStatusBar(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowCaptioning(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowCaptioning(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_ShowAudioControls(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_ShowAudioControls(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_CaptioningID(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_CaptioningID(
          IMediaPlayer* iface,
          BSTR pstrText)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_Mute(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_Mute(
          IMediaPlayer* iface,
          VARIANT_BOOL vbool)
{
    TRACE("\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_CanPreview(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_PreviewMode(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_PreviewMode(
          IMediaPlayer* iface,
          VARIANT_BOOL pPreviewMode)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_HasMultipleItems(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_Language(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_Language(
          IMediaPlayer* iface,
          long pLanguage)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_AudioStream(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AudioStream(
          IMediaPlayer* iface,
          long pStream)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_SAMIStyle(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SAMIStyle(
          IMediaPlayer* iface,
          BSTR pbstrStyle)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_SAMILang(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SAMILang(
          IMediaPlayer* iface,
          BSTR pbstrLang)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_get_SAMIFileName(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_SAMIFileName(
          IMediaPlayer* iface,
          BSTR pbstrFileName)
{
    FIXME("is not implemented\n");
}

static long WINAPI MediaPlayer_get_StreamCount(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_get_ClientId(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_get_ConnectionSpeed(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_AutoSize(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_AutoSize(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_get_EnableFullScreenControls(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_EnableFullScreenControls(
          IMediaPlayer* iface,
          VARIANT_BOOL pbVal)
{
    FIXME("is not implemented\n");
}

static IDispatch * WINAPI MediaPlayer_get_ActiveMovie(
                 IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static IDispatch * WINAPI MediaPlayer_get_NSPlay(
                 IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_get_WindowlessVideo(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_put_WindowlessVideo(
          IMediaPlayer* iface,
          VARIANT_BOOL pbool)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_Play(
          IMediaPlayer* iface)
{
    WCHAR command[MAX_PATH] = {'c',':','\\','w','i','n','d','o','w','s','\\',
        'c','o','m','m','a','n','d','\\','m','p','l','a','y','e','r',' ',0};
    Player *This = MEDIAPLAYER_THIS(iface);

    if(This->FileName[0] != '\0')
    {
        TRACE("Play %s\n", debugstr_w(This->FileName));
        lstrcatW(command, This->FileName);
        ShellExecuteW(0,0, command, 0, 0, 0);
    }
    else
        WARN("Nothing to play\n");
}

static void WINAPI MediaPlayer_Stop(
          IMediaPlayer* iface)
{
    FIXME("stub\n");
}

static void WINAPI MediaPlayer_Pause(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static double WINAPI MediaPlayer_GetMarkerTime(
            IMediaPlayer* iface,
            long MarkerNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_GetMarkerName(
          IMediaPlayer* iface,
          long MarkerNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_AboutBox(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static VARIANT_BOOL WINAPI MediaPlayer_GetCodecInstalled(
                  IMediaPlayer* iface,
                  long CodecNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_GetCodecDescription(
          IMediaPlayer* iface,
          long CodecNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_GetCodecURL(
          IMediaPlayer* iface,
          long CodecNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_GetMoreInfoURL(
          IMediaPlayer* iface,
          MPMoreInfoType MoreInfoType)
{
    FIXME("is not implemented\n");
    return 0;
}

static BSTR WINAPI MediaPlayer_GetMediaInfoString(
          IMediaPlayer* iface,
          MPMediaInfoType MediaInfoType)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_Cancel(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_Open(
          IMediaPlayer* iface,
          BSTR bstrFileName)
{
    Player *This = MEDIAPLAYER_THIS(iface);
    TRACE("%s\n", debugstr_w(bstrFileName));
    lstrcpyW(This->FileName, bstrFileName);
}

static VARIANT_BOOL WINAPI MediaPlayer_IsSoundCardEnabled(
                  IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
    return 0;
}

static void WINAPI MediaPlayer_Next(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_Previous(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_StreamSelect(
          IMediaPlayer* iface,
          long StreamNum)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_FastForward(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static void WINAPI MediaPlayer_FastReverse(
          IMediaPlayer* iface)
{
    FIXME("is not implemented\n");
}

static BSTR WINAPI MediaPlayer_GetStreamName(
          IMediaPlayer* iface,
          long StreamNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static long WINAPI MediaPlayer_GetStreamGroup(
          IMediaPlayer* iface,
          long StreamNum)
{
    FIXME("is not implemented\n");
    return 0;
}

static VARIANT_BOOL WINAPI MediaPlayer_GetStreamSelected(
                  IMediaPlayer* iface,
                  long StreamNum)
{
    FIXME("is not implemented\n");
    return 0;
}


const IMediaPlayerVtbl MediaPlayerVtbl =
{
    MediaPlayer_QueryInterface,
    MediaPlayer_AddRef,
    MediaPlayer_Release,
    MediaPlayer_GetTypeInfoCount,
    MediaPlayer_GetTypeInfo,
    MediaPlayer_GetIDsOfNames,
    MediaPlayer_Invoke,
    MediaPlayer_get_CurrentPosition,
    MediaPlayer_put_CurrentPosition,
    MediaPlayer_get_Duration,
    MediaPlayer_get_ImageSourceWidth,
    MediaPlayer_get_ImageSourceHeight,
    MediaPlayer_get_MarkerCount,
    MediaPlayer_get_CanScan,
    MediaPlayer_get_CanSeek,
    MediaPlayer_get_CanSeekToMarkers,
    MediaPlayer_get_CurrentMarker,
    MediaPlayer_put_CurrentMarker,
    MediaPlayer_get_FileName,
    MediaPlayer_put_FileName,
    MediaPlayer_get_SourceLink,
    MediaPlayer_get_CreationDate,
    MediaPlayer_get_ErrorCorrection,
    MediaPlayer_get_Bandwidth,
    MediaPlayer_get_SourceProtocol,
    MediaPlayer_get_ReceivedPackets,
    MediaPlayer_get_RecoveredPackets,
    MediaPlayer_get_LostPackets,
    MediaPlayer_get_ReceptionQuality,
    MediaPlayer_get_BufferingCount,
    MediaPlayer_get_IsBroadcast,
    MediaPlayer_get_BufferingProgress,
    MediaPlayer_get_ChannelName,
    MediaPlayer_get_ChannelDescription,
    MediaPlayer_get_ChannelURL,
    MediaPlayer_get_ContactAddress,
    MediaPlayer_get_ContactPhone,
    MediaPlayer_get_ContactEmail,
    MediaPlayer_get_BufferingTime,
    MediaPlayer_put_BufferingTime,
    MediaPlayer_get_AutoStart,
    MediaPlayer_put_AutoStart,
    MediaPlayer_get_AutoRewind,
    MediaPlayer_put_AutoRewind,
    MediaPlayer_get_Rate,
    MediaPlayer_put_Rate,
    MediaPlayer_get_SendKeyboardEvents,
    MediaPlayer_put_SendKeyboardEvents,
    MediaPlayer_get_SendMouseClickEvents,
    MediaPlayer_put_SendMouseClickEvents,
    MediaPlayer_get_SendMouseMoveEvents,
    MediaPlayer_put_SendMouseMoveEvents,
    MediaPlayer_get_PlayCount,
    MediaPlayer_put_PlayCount,
    MediaPlayer_get_ClickToPlay,
    MediaPlayer_put_ClickToPlay,
    MediaPlayer_get_AllowScan,
    MediaPlayer_put_AllowScan,
    MediaPlayer_get_EnableContextMenu,
    MediaPlayer_put_EnableContextMenu,
    MediaPlayer_get_CursorType,
    MediaPlayer_put_CursorType,
    MediaPlayer_get_CodecCount,
    MediaPlayer_get_AllowChangeDisplaySize,
    MediaPlayer_put_AllowChangeDisplaySize,
    MediaPlayer_get_IsDurationValid,
    MediaPlayer_get_OpenState,
    MediaPlayer_get_SendOpenStateChangeEvents,
    MediaPlayer_put_SendOpenStateChangeEvents,
    MediaPlayer_get_SendWarningEvents,
    MediaPlayer_put_SendWarningEvents,
    MediaPlayer_get_SendErrorEvents,
    MediaPlayer_put_SendErrorEvents,
    MediaPlayer_get_PlayState,
    MediaPlayer_get_SendPlayStateChangeEvents,
    MediaPlayer_put_SendPlayStateChangeEvents,
    MediaPlayer_get_DisplaySize,
    MediaPlayer_put_DisplaySize,
    MediaPlayer_get_InvokeURLs,
    MediaPlayer_put_InvokeURLs,
    MediaPlayer_get_BaseURL,
    MediaPlayer_put_BaseURL,
    MediaPlayer_get_DefaultFrame,
    MediaPlayer_put_DefaultFrame,
    MediaPlayer_get_HasError,
    MediaPlayer_get_ErrorDescription,
    MediaPlayer_get_ErrorCode,
    MediaPlayer_get_AnimationAtStart,
    MediaPlayer_put_AnimationAtStart,
    MediaPlayer_get_TransparentAtStart,
    MediaPlayer_put_TransparentAtStart,
    MediaPlayer_get_Volume,
    MediaPlayer_put_Volume,
    MediaPlayer_get_Balance,
    MediaPlayer_put_Balance,
    MediaPlayer_get_ReadyState,
    MediaPlayer_get_SelectionStart,
    MediaPlayer_put_SelectionStart,
    MediaPlayer_get_SelectionEnd,
    MediaPlayer_put_SelectionEnd,
    MediaPlayer_get_ShowDisplay,
    MediaPlayer_put_ShowDisplay,
    MediaPlayer_get_ShowControls,
    MediaPlayer_put_ShowControls,
    MediaPlayer_get_ShowPositionControls,
    MediaPlayer_put_ShowPositionControls,
    MediaPlayer_get_ShowTracker,
    MediaPlayer_put_ShowTracker,
    MediaPlayer_get_EnablePositionControls,
    MediaPlayer_put_EnablePositionControls,
    MediaPlayer_get_EnableTracker,
    MediaPlayer_put_EnableTracker,
    MediaPlayer_get_Enabled,
    MediaPlayer_put_Enabled,
    MediaPlayer_get_DisplayForeColor,
    MediaPlayer_put_DisplayForeColor,
    MediaPlayer_get_DisplayBackColor,
    MediaPlayer_put_DisplayBackColor,
    MediaPlayer_get_DisplayMode,
    MediaPlayer_put_DisplayMode,
    MediaPlayer_get_VideoBorder3D,
    MediaPlayer_put_VideoBorder3D,
    MediaPlayer_get_VideoBorderWidth,
    MediaPlayer_put_VideoBorderWidth,
    MediaPlayer_get_VideoBorderColor,
    MediaPlayer_put_VideoBorderColor,
    MediaPlayer_get_ShowGotoBar,
    MediaPlayer_put_ShowGotoBar,
    MediaPlayer_get_ShowStatusBar,
    MediaPlayer_put_ShowStatusBar,
    MediaPlayer_get_ShowCaptioning,
    MediaPlayer_put_ShowCaptioning,
    MediaPlayer_get_ShowAudioControls,
    MediaPlayer_put_ShowAudioControls,
    MediaPlayer_get_CaptioningID,
    MediaPlayer_put_CaptioningID,
    MediaPlayer_get_Mute,
    MediaPlayer_put_Mute,
    MediaPlayer_get_CanPreview,
    MediaPlayer_get_PreviewMode,
    MediaPlayer_put_PreviewMode,
    MediaPlayer_get_HasMultipleItems,
    MediaPlayer_get_Language,
    MediaPlayer_put_Language,
    MediaPlayer_get_AudioStream,
    MediaPlayer_put_AudioStream,
    MediaPlayer_get_SAMIStyle,
    MediaPlayer_put_SAMIStyle,
    MediaPlayer_get_SAMILang,
    MediaPlayer_put_SAMILang,
    MediaPlayer_get_SAMIFileName,
    MediaPlayer_put_SAMIFileName,
    MediaPlayer_get_StreamCount,
    MediaPlayer_get_ClientId,
    MediaPlayer_get_ConnectionSpeed,
    MediaPlayer_get_AutoSize,
    MediaPlayer_put_AutoSize,
    MediaPlayer_get_EnableFullScreenControls,
    MediaPlayer_put_EnableFullScreenControls,
    MediaPlayer_get_ActiveMovie,
    MediaPlayer_get_NSPlay,
    MediaPlayer_get_WindowlessVideo,
    MediaPlayer_put_WindowlessVideo,
    MediaPlayer_Play,
    MediaPlayer_Stop,
    MediaPlayer_Pause,
    MediaPlayer_GetMarkerTime,
    MediaPlayer_GetMarkerName,
    MediaPlayer_AboutBox,
    MediaPlayer_GetCodecInstalled,
    MediaPlayer_GetCodecDescription,
    MediaPlayer_GetCodecURL,
    MediaPlayer_GetMoreInfoURL,
    MediaPlayer_GetMediaInfoString,
    MediaPlayer_Cancel,
    MediaPlayer_Open,
    MediaPlayer_IsSoundCardEnabled,
    MediaPlayer_Next,
    MediaPlayer_Previous,
    MediaPlayer_StreamSelect,
    MediaPlayer_FastForward,
    MediaPlayer_FastReverse,
    MediaPlayer_GetStreamName,
    MediaPlayer_GetStreamGroup,
    MediaPlayer_GetStreamSelected
};

#undef MEDIAPLAYER_THIS

void Player_MediaPlayer_Init(Player *This)
{
    This->lpMediaPlayerVtbl = &MediaPlayerVtbl;
}
