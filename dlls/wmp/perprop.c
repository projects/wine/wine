/*
 * Implementation of IPerPropertyBrowsing interface
 *
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdio.h>

#include "eterwmp.h"
#include "windef.h"
#include "winbase.h"
#include "wmp.h"

/**********************************************************
 * IPerPropertyBrowsing methods implementation
 */

#define PROPBROWSING_THIS(iface) DEFINE_THIS(Player, PerPropertyBrowsing, iface)

static HRESULT WINAPI PerPropertyBrowsing_QueryInterface(IPerPropertyBrowsing *iface,
        REFIID riid, LPVOID *ppvObject)
{
    Player *This = PROPBROWSING_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PerPropertyBrowsing_AddRef(IPerPropertyBrowsing *iface)
{
    Player *This = PROPBROWSING_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PerPropertyBrowsing_Release(IPerPropertyBrowsing *iface)
{
    Player *This = PROPBROWSING_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI PerPropertyBrowsing_GetDisplayString(
             IPerPropertyBrowsing* This,
             DISPID dispID,
             BSTR *pBstr)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PerPropertyBrowsing_MapPropertyToPage(
             IPerPropertyBrowsing* This,
             DISPID dispID,
             CLSID *pClsid)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PerPropertyBrowsing_GetPredefinedStrings(
             IPerPropertyBrowsing* This,
             DISPID dispID,
             CALPOLESTR *pCaStringsOut,
             CADWORD *pCaCookiesOut)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI PerPropertyBrowsing_GetPredefinedValue(
             IPerPropertyBrowsing* This,
             DISPID dispID,
             DWORD dwCookie,
             VARIANT *pVarOut)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static const IPerPropertyBrowsingVtbl PerPropertyBrowsingVtbl =
{
    PerPropertyBrowsing_QueryInterface,
    PerPropertyBrowsing_AddRef,
    PerPropertyBrowsing_Release,
    PerPropertyBrowsing_GetDisplayString,
    PerPropertyBrowsing_MapPropertyToPage,
    PerPropertyBrowsing_GetPredefinedStrings,
    PerPropertyBrowsing_GetPredefinedValue
};

#undef PROPBROWSING_THIS

void Player_PerProperty_Init(Player *This)
{
    This->lpPerPropertyBrowsingVtbl = &PerPropertyBrowsingVtbl;
}
