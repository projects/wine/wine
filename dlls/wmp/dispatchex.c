/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include "wmpids.h"

/**********************************************************
 * IDispatchEx methods implementation
 */

#define DISPEX_THIS(iface) DEFINE_THIS(Player, DispatchEx, iface)

static HRESULT WINAPI DispatchEx_QueryInterface(
        IDispatchEx* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = DISPEX_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI DispatchEx_AddRef(IDispatchEx* iface)
{
    Player *This = DISPEX_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI DispatchEx_Release(IDispatchEx* iface)
{
    Player *This = DISPEX_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI DispatchEx_GetTypeInfoCount(
        IDispatchEx* iface,
        UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetTypeInfo(
        IDispatchEx* iface,
        UINT iTInfo,
        LCID lcid,
        ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetIDsOfNames(
        IDispatchEx* iface,
        REFIID riid,
        LPOLESTR *rgszNames,
        UINT cNames,
        LCID lcid,
        DISPID *rgDispId)
{
    const WCHAR str_controls[] = {'c', 'o', 'n', 't', 'r', 'o' ,'l', 's', 0};
    const WCHAR str_playstate[] = {'p', 'l', 'a', 'y', 'S', 't' ,'a', 't', 'e', 0};
    const WCHAR str_isAvailable[] = {'i', 's', 'A', 'v', 'a', 'i' ,'l', 'a', 'b', 'l', 'e', 0};
    const WCHAR str_play[] = {'p', 'l', 'a', 'y', 0};

    /* controls */
    if (!lstrcmpiW(*rgszNames, str_controls)) {
        TRACE(" - DISPID_WMPCORE_CONTROLS\n");
        *rgDispId = DISPID_WMPCORE_CONTROLS;
        return S_OK;
    }
    /* playState */
    if (!lstrcmpiW(*rgszNames, str_playstate)) {
        TRACE(" - DISPID_WMPCORE_PLAYSTATE\n");
        *rgDispId = DISPID_WMPCORE_PLAYSTATE;
        return S_OK;
    }
    /* isAvailable */
    if (!lstrcmpiW(*rgszNames, str_isAvailable)) {
        TRACE("returns DISPID_WMPCONTROLS_ISAVAILABLE\n");
        *rgDispId = DISPID_WMPCONTROLS_ISAVAILABLE;
        return S_OK;
    }
    /* play */
    if (!lstrcmpiW(*rgszNames, str_play)) {
        TRACE("returns DISPID_WMPCONTROLS_PLAY\n");
        *rgDispId = DISPID_WMPCONTROLS_PLAY;
        return S_OK;
    }
    FIXME("Need to implement %s\n", debugstr_w(*rgszNames));
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_Invoke(
        IDispatchEx* iface,
        DISPID dispIdMember,
        REFIID riid,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pDispParams,
        VARIANT *pVarResult,
        EXCEPINFO *pExcepInfo,
        UINT *puArgErr)
{
    HRESULT res;
    Player *This = DISPEX_THIS(iface);
    IWMPControls *controls;
    IWMPPlayer *player = NULL;
    WMPOpenState openstate;

    TRACE("call to %d %d\n", dispIdMember, wFlags);

    switch(dispIdMember)
    {
        case -5511:
            TRACE("return S_OK\n");
            VariantInit(pVarResult);
            V_VT(pVarResult) = VT_BOOL;
            V_BOOL(pVarResult) = VARIANT_TRUE;
            return S_OK;

        case DISPID_WMPCORE_URL: /* 1 */
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to put_URL\n");
                res = IWMPCore_put_URL(CORE(This), V_BSTR(&pDispParams->rgvarg[0]));
                if(FAILED(res)) return res;
                return S_OK;
            }
            break;

        case DISPID_WMPCORE_OPENSTATE: /* 2 */
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to get_openState\n");
                res = IWMPCore_get_openState(CORE(This), &openstate);
                if(FAILED(res)) return res;
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_I8;
                V_I8(pVarResult) = (WMPOpenState) openstate;
                return S_OK;
            }
            break;

        case DISPID_WMPCORE_CONTROLS: /* 4 */
            TRACE("call to get_controls\n");
            res = IWMPCore_QueryInterface(CORE(This), &IID_IWMPControls, (LPVOID*) &controls);
            if(FAILED(res)) return res;
            VariantInit(pVarResult);
            V_VT(pVarResult) = VT_DISPATCH;
            V_DISPATCH(pVarResult) = (IDispatch*) controls;
            return S_OK;

        case DISPID_WMPCORE_PLAYSTATE: /* 10 */
            TRACE("call to get_playState\n");
            VariantInit(pVarResult);
            V_VT(pVarResult) = VT_I8;
            V_I8(pVarResult) = (WMPPlayState) This->playState;
            return S_OK;

        case DISPID_WMPOCX_ENABLED: /* 19 */
            res = IWMPCore_QueryInterface(CORE(This), &IID_IWMPPlayer, (LPVOID*) &player);
            if(FAILED(res)) return res;
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to get_enabled\n");
                VariantInit(pVarResult);
                res = IWMPPlayer_get_enabled(player, (VARIANT_BOOL*)pVarResult);
                IWMPCore_Release(CORE(This));
                return res;
            }
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to put_enabled\n");
                res = IWMPPlayer_put_enabled(player, V_BOOL(&pDispParams->rgvarg[0]));
                IWMPCore_Release(CORE(This));
                return res;
            }
            IWMPCore_Release(CORE(This));
            break;

        case DISPID_WMPCONTROLS_PLAY: /* 51 */
            TRACE("call to play\n");
            res = IWMPCore_QueryInterface(CORE(This), &IID_IWMPControls, (LPVOID*) &controls);
            if(FAILED(res)) return res;
            res = IWMPControls_play((IWMPControls*)controls);
            IWMPCore_Release(CORE(This));
            return res;

        case DISPID_WMPCONTROLS_ISAVAILABLE: /* 62 */
            TRACE("call to isAvailable\n");
            VariantInit(pVarResult);
            V_VT(pVarResult) = VT_BOOL;
            V_BOOL(pVarResult) = VARIANT_TRUE;
            return S_OK;

        /* Invoke IMedeaPlayer methods */
        case -514:
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to IMediaPlayer_get_Enabled\n");
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_BOOL;
                V_BOOL(pVarResult) = IMediaPlayer_get_Enabled(MEDIAPLAYER(This));
                return S_OK;
            }
            break;

        case -525:
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to IMediaPlayer_get_ReadyState\n");
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_I8;
                V_I8(pVarResult) = IMediaPlayer_get_ReadyState(MEDIAPLAYER(This));
                return S_OK;
                }
            break;

        case 32: /* DisplayMode */
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                This->DisplayMode = (MPDisplayModeConstants) V_I8(&pDispParams->rgvarg[0]);
                TRACE("set DisplayMode %d\n", This->DisplayMode);
                return S_OK;
            }
            break;

        case 1003: /* FileName */
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("get Duration - %lf\n", This->Duration);
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_R8;
                V_R8(pVarResult) = This->Duration;
                return S_OK;
            }

        case 1017: /* Autostart */
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to IMediaPlayer_put_AutoStart\n");
                IMediaPlayer_put_AutoStart(MEDIAPLAYER(This), V_BOOL(&pDispParams->rgvarg[0]));
                return S_OK;
            }
            break;

        case 1026: /* FileName */
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to IMediaPlayer_get_FileName\n");
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_BSTR;
                V_BSTR(pVarResult) = IMediaPlayer_get_FileName(MEDIAPLAYER(This));
                return S_OK;
            }
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to IMediaPlayer_put_FileName\n");
                IMediaPlayer_put_FileName(MEDIAPLAYER(This), V_BSTR(&pDispParams->rgvarg[0]));
                return S_OK;
            }
            break;

        case 1027: /* CurrentPosition */
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to IMediaPlayer_get_CurrentPosition\n");
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_R8;
                V_R8(pVarResult) = IMediaPlayer_get_CurrentPosition(MEDIAPLAYER(This));
                return S_OK;
            }
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to IMediaPlayer_put_CurrentPosition\n");
                IMediaPlayer_put_CurrentPosition(MEDIAPLAYER(This), V_R8(&pDispParams->rgvarg[0]));
                return S_OK;
            }
            break;

        case 1068: /* playState*/
            if(wFlags & DISPATCH_PROPERTYGET)
            {
                TRACE("call to IMediaPlayer_get_PlayState\n");
                VariantInit(pVarResult);
                V_VT(pVarResult) = VT_I8;
                V_I8(pVarResult) = IMediaPlayer_get_PlayState(MEDIAPLAYER(This));
                return S_OK;
            }
            break;
        case 1089: /* Autostart*/
            if(wFlags & DISPATCH_PROPERTYPUT)
            {
                TRACE("call to IMediaPlayer_put_Mute\n");
                IMediaPlayer_put_Mute(MEDIAPLAYER(This), V_BOOL(&pDispParams->rgvarg[0]));
                return S_OK;
            }
            break;

    }
    FIXME("call to undefined function\n");
    return E_NOTIMPL;
}

    /*** IDispatchEx methods ***/
static HRESULT WINAPI DispatchEx_GetDispID(
        IDispatchEx* iface,
        BSTR bstrName,
        DWORD grfdex,
        DISPID *pid)
{
    Player *This = DISPEX_THIS(iface);
    TRACE("\n");
    return DispatchEx_GetIDsOfNames(DISPATCHEX(This), &IID_IDispatchEx, &bstrName, grfdex, 0, pid);
}

static HRESULT WINAPI DispatchEx_InvokeEx(
        IDispatchEx* iface,
        DISPID id,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pdp,
        VARIANT *pvarRes,
        EXCEPINFO *pei,
        IServiceProvider *pspCaller)
{
    Player *This = DISPEX_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), id, &IID_IDispatchEx, lcid, wFlags, pdp, pvarRes, pei, NULL);
}

static HRESULT WINAPI DispatchEx_DeleteMemberByName(
        IDispatchEx* iface,
        BSTR bstrName,
        DWORD grfdex)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_DeleteMemberByDispID(
        IDispatchEx* iface,
        DISPID id)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetMemberProperties(
        IDispatchEx* iface,
        DISPID id,
        DWORD grfdexFetch,
        DWORD *pgrfdex)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetMemberName(
        IDispatchEx* iface,
        DISPID id,
        BSTR *pbstrName)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetNextDispID(
        IDispatchEx* iface,
        DWORD grfdex,
        DISPID id,
        DISPID *pid)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI DispatchEx_GetNameSpaceParent(
        IDispatchEx* iface,
        IUnknown **ppunk)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static const IDispatchExVtbl DispatchExVtbl =
{
    DispatchEx_QueryInterface,
    DispatchEx_AddRef,
    DispatchEx_Release,
    DispatchEx_GetTypeInfoCount,
    DispatchEx_GetTypeInfo,
    DispatchEx_GetIDsOfNames,
    DispatchEx_Invoke,
    DispatchEx_GetDispID,
    DispatchEx_InvokeEx,
    DispatchEx_DeleteMemberByName,
    DispatchEx_DeleteMemberByDispID,
    DispatchEx_GetMemberProperties,
    DispatchEx_GetMemberName,
    DispatchEx_GetNextDispID,
    DispatchEx_GetNameSpaceParent
};

#undef DISPEX_THIS

void Player_DispatchEx_Init(Player *This)
{
    This->lpDispatchExVtbl = &DispatchExVtbl;
}
