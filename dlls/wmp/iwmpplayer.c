/*
 * IWMPCore, IWMPPlayer and IWMPPlayer2 interfaces functions
 *
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include <shellapi.h>
#include "wmpids.h"

/* Implementation of IWMPCore interface */

#define CORE_THIS(iface) DEFINE_THIS(Player, WMPCore, iface)

    /*** IUnknown methods ***/
static HRESULT WINAPI WMPCore_QueryInterface(
                                             IWMPCore* iface,
                                             REFIID riid,
                                             void **ppvObject)
{
    Player *This = CORE_THIS(iface);

    *ppvObject = NULL;
    if(IsEqualGUID(&IID_IUnknown, riid)) {
        TRACE("(%p)->(IID_IUnknown, %p)\n", This, ppvObject);
        *ppvObject = CORE(This);
    }else if(IsEqualGUID(&IID_IDispatch, riid)) {
        TRACE("(%p)->(IID_IDispatch, %p)\n", This, ppvObject);
        *ppvObject = DISPATCHEX(This);
    }else if(IsEqualGUID(&IID_IDispatchEx, riid)) {
        TRACE("(%p)->(IID_IDispatchEx, %p)\n", This, ppvObject);
        *ppvObject = DISPATCHEX(This);
    }else if(IsEqualGUID(&IID_IWMPCore, riid)) {
        TRACE("(%p)->(IID_IWMPCore, %p)\n", This, ppvObject);
        *ppvObject = CORE(This);
    }else if(IsEqualGUID(&IID_IWMPPlayer, riid)) {
        TRACE("(%p)->(IID_IWMPPlayer, %p)\n", This, ppvObject);
        *ppvObject = PLAYER(This);
    }else if(IsEqualGUID(&IID_IWMPPlayer2, riid)) {
        TRACE("(%p)->(IID_IWMPPlayer2, %p)\n", This, ppvObject);
        *ppvObject = PLAYER2(This);
    }else if(IsEqualGUID(&IID_IWMPControls, riid)) {
        TRACE("(%p)->(IID_IWMPControls, %p)\n", This, ppvObject);
        *ppvObject = CONTROLS(This);
    }else if(IsEqualGUID(&IID_IWMPSettings, riid)) {
        TRACE("(%p)->(IID_IWMPSettings, %p)\n", This, ppvObject);
        *ppvObject = SETTINGS(This);
    }else if(IsEqualGUID(&IID_IWMPError, riid)) {
        TRACE("(%p)->(IID_IWMPError, %p)\n", This, ppvObject);
        *ppvObject = WMPERROR(This);
    }else if(IsEqualGUID(&IID_IMediaPlayer, riid)) {
        TRACE("(%p)->(IID_IMediaPlayer, %p)\n", This, ppvObject);
        *ppvObject = MEDIAPLAYER(This);
    }else if(IsEqualGUID(&IID_IOleObject, riid)) {
        TRACE("(%p)->(IID_IOleObject, %p)\n", This, ppvObject);
        *ppvObject = OLEOBJ(This);
    }else if(IsEqualGUID(&IID_IOleControl, riid)) {
        TRACE("(%p)->(IID_IOleControl, %p)\n", This, ppvObject);
        *ppvObject = CONTROL(This);
    }else if(IsEqualGUID(&IID_IOleCommandTarget, riid)) {
        TRACE("(%p)->(IID_IOleCommandTarget, %p)\n", This, ppvObject);
        *ppvObject = CMDTARGET(This);
    }else if(IsEqualGUID(&IID_IPersist, riid)) {
        TRACE("(%p)->(IID_IPersist, %p)\n", This, ppvObject);
        *ppvObject = PERSIST(This);
    }else if(IsEqualGUID(&IID_IMediaPlayer, riid)) {
        TRACE("(%p)->(IID_IPersistStream, %p)\n", This, ppvObject);
        *ppvObject = PERSISTSTREAM(This);
    }else if(IsEqualGUID(&IID_IPersistStreamInit, riid)) {
        TRACE("(%p)->(IID_IPersistStreamInit, %p)\n", This, ppvObject);
        *ppvObject = PERSISTSTREAMINIT(This);
    }else if(IsEqualGUID(&IID_IPersistPropertyBag, riid)) {
        TRACE("(%p)->(IID_IPersistPropertyBag, %p)\n", This, ppvObject);
        *ppvObject = PROPBAG(This);
    }else if(IsEqualGUID(&IID_IPersistPropertyBag2, riid)) {
        TRACE("(%p)->(IID_IPersistPropertyBag2, %p)\n", This, ppvObject);
        *ppvObject = PROPBAG2(This);
    }else if(IsEqualGUID(&IID_IPerPropertyBrowsing, riid)) {
        TRACE("(%p)->(IID_IPerPropertyBrowsing, %p)\n", This, ppvObject);
        *ppvObject = PROPBROWSING(This);
    }else if(IsEqualGUID(&IID_IPersistHistory, riid)) {
        TRACE("(%p)->(IID_IPersistHistory, %p)\n", This, ppvObject);
        *ppvObject = HISTORY(This);
    }else if(IsEqualGUID(&IID_IConnectionPointContainer, riid)) {
        TRACE("(%p)->(IID_IConnectionPointContainer, %p)\n", This, ppvObject);
        *ppvObject = CPCONT(This);
    }else if(IsEqualGUID(&IID_IConnectionPoint, riid)) {
        TRACE("(%p)->(IID_IConnectionPoint, %p)\n", This, ppvObject);
        *ppvObject = CPOINT(This);
    }else if(IsEqualGUID(&IID_IPointerInactive, riid)) {
        TRACE("(%p)->(IID_IPointerInactive, %p)\n", This, ppvObject);
        *ppvObject = INACTIVE(This);
    }else if(IsEqualGUID(&IID_IObjectSafety, riid)) {
        TRACE("(%p)->(IID_IObjectSafety, %p)\n", This, ppvObject);
        *ppvObject = SAFETY(This);
    }else if(IsEqualGUID(&IID_IRunnableObject, riid)) {
        TRACE("(%p)->(IID_IRunnableObject, %p)\n", This, ppvObject);
        *ppvObject = RUNOBJ(This);
    }else if(IsEqualGUID(&IID_IViewObject, riid)) {
        TRACE("(%p)->(IID_IViewObject, %p)\n", This, ppvObject);
        *ppvObject = VIEWOBJ(This);
    }else if(IsEqualGUID(&IID_IViewObjectEx, riid)) {
        TRACE("(%p)->(IID_IViewObjectEx, %p)\n", This, ppvObject);
        *ppvObject = VIEWOBJEX(This);
    }else if(IsEqualGUID(&IID_IClientSecurity, riid)) {
        TRACE("(%p)->(IID_IClientSecurity, %p)\n", This, ppvObject);
        *ppvObject = SECURITY(This);
    }else if(IsEqualGUID(&IID_IOleWindow, riid)) {
        TRACE("(%p)->(IID_IOleWindow, %p)\n", This, ppvObject);
        *ppvObject = OLEWINDOW(This);
    }else if(IsEqualGUID(&IID_IOleInPlaceObject, riid)) {
        TRACE("(%p)->(IID_IOleInPlaceObject, %p)\n", This, ppvObject);
        *ppvObject = INPLACE(This);
    }else if(IsEqualGUID(&IID_IOleInPlaceObjectWindowless, riid)) {
        TRACE("(%p)->(IID_IOleInPlaceObjectWindowless, %p)\n", This, ppvObject);
        *ppvObject = INPLACEW(This);
    }
    if(*ppvObject) {
        IWMPPlayer_AddRef(iface);
        return S_OK;
    }
    FIXME("(%p)->(%s %p) interface not supported\n", This, debugstr_guid(riid), ppvObject);
    return E_NOINTERFACE;
}

static ULONG WINAPI WMPCore_AddRef(IWMPCore* iface)
{
    Player *This = CORE_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI WMPCore_Release(IWMPCore* iface)
{
    Player *This = CORE_THIS(iface);
    ULONG ref = InterlockedDecrement(&This->ref);

    TRACE("(%p) ref = %u\n", This, ref);

    if(!ref) {
        if(This->client)
            IOleObject_SetClientSite(OLEOBJ(This), NULL);

        InterlockedDecrement(&dll_ref);
        HeapFree(GetProcessHeap(), 0, This);
    }
    return ref;
}

   /*** IDispatch methods ***/
static HRESULT WINAPI WMPCore_GetTypeInfoCount(
                                               IWMPCore* iface,
                                               UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_GetTypeInfo(
                                          IWMPCore* iface,
                                          UINT iTInfo,
                                          LCID lcid,
                                          ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_GetIDsOfNames(
                                            IWMPCore* iface,
                                            REFIID riid,
                                            LPOLESTR *rgszNames,
                                            UINT cNames,
                                            LCID lcid,
                                            DISPID *rgDispId)
{
    Player *This = CORE_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}

static HRESULT WINAPI WMPCore_Invoke(
                                     IWMPCore* iface,
                                     DISPID dispIdMember,
                                     REFIID riid,
                                     LCID lcid,
                                     WORD wFlags,
                                     DISPPARAMS *pDispParams,
                                     VARIANT *pVarResult,
                                     EXCEPINFO *pExcepInfo,
                                     UINT *puArgErr)
{
    Player *This = CORE_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPCore methods ***/
static HRESULT WINAPI WMPCore_close(
                                    IWMPCore* iface)
{
    FIXME("stub\n");
    return S_OK;
}

static HRESULT WINAPI WMPCore_get_URL(
                                      IWMPCore* iface,
                                      BSTR *pbstrURL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_put_URL(
                                      IWMPCore* iface,
                                      BSTR bstrURL)
{
    TRACE("%s\n", debugstr_w(bstrURL));
    ShellExecuteW(0,0,bstrURL,0,0,0);
    return S_OK;
}

static HRESULT WINAPI WMPCore_get_openState(
                                            IWMPCore* iface,
                                            WMPOpenState *pwmpos)
{
    TRACE("returns wmposMediaOpen\n");
    *pwmpos = wmposMediaOpen;
    return S_OK;
}

static HRESULT WINAPI WMPCore_get_playState(
                                            IWMPCore* iface,
                                            WMPPlayState *pwmpps)
{
    Player *This = CORE_THIS(iface);
    TRACE("\n");
    *pwmpps = This->playState;
    return S_OK;
}

static HRESULT WINAPI WMPCore_get_controls(
                                           IWMPCore* iface,
                                           IWMPControls **ppControl)
{
    Player *This = CORE_THIS(iface);
    HRESULT hres;

    TRACE("\n");

    hres = IWMPCore_QueryInterface(CORE(This), &IID_IWMPControls, (LPVOID) ppControl);
    return hres;
}

static HRESULT WINAPI WMPCore_get_settings(
                                           IWMPCore* iface,
                                           IWMPSettings **ppSettings)
{
    Player *This = CORE_THIS(iface);
    HRESULT hres;

    TRACE("\n");

    hres = IWMPCore_QueryInterface(CORE(This), &IID_IWMPSettings, (LPVOID) ppSettings);
    return hres;
}

static HRESULT WINAPI WMPCore_get_currentMedia(
                                               IWMPCore* iface,
                                               IWMPMedia **ppMedia)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_put_currentMedia(
                                               IWMPCore* iface,
                                               IWMPMedia *pMedia)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_mediaCollection(
        IWMPCore* iface,
        IWMPMediaCollection **ppMediaCollection)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_playlistCollection(
        IWMPCore* iface,
        IWMPPlaylistCollection **ppPlaylistCollection)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_versionInfo(
                                              IWMPCore* iface,
                                              BSTR *pbstrVersionInfo)
{
    static WCHAR versionInfo[] = {'9','.','0','0','.','0','0','.','3','3','5','4'};
    TRACE("return 9.00.00.3354\n");
    *pbstrVersionInfo = SysAllocString(versionInfo);
//    *pbstrVersionInfo = SysAllocString(OLESTR("9.00.00.3354"));
//     TRACE("return 6.4.9.0\n");
//     *pbstrVersionInfo = SysAllocString(OLESTR("6.4.9.0"));

    return S_OK;
}

static HRESULT WINAPI WMPCore_launchURL(
                                        IWMPCore* iface,
                                        BSTR bstrURL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_network(
                                          IWMPCore* iface,
                                          IWMPNetwork **ppQNI)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_currentPlaylist(
        IWMPCore* iface,
        IWMPPlaylist **ppPL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_put_currentPlaylist(
        IWMPCore* iface,
        IWMPPlaylist *pPL)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_cdromCollection(
        IWMPCore* iface,
        IWMPCdromCollection **ppCdromCollection)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_closedCaption(
        IWMPCore* iface,
        IWMPClosedCaption **ppClosedCaption)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_isOnline(
                                           IWMPCore* iface,
                                           VARIANT_BOOL *pfOnline)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPCore_get_error(
                                        IWMPCore* iface,
                                        IWMPError **ppError)
{
    IWMPCore_QueryInterface(iface, &IID_IWMPError, (LPVOID*) ppError);
    return S_OK;
}

static HRESULT WINAPI WMPCore_get_status(
                                         IWMPCore* iface,
                                         BSTR *pbstrStatus)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

    const IWMPCoreVtbl WMPCoreVtbl =
{
    WMPCore_QueryInterface,
    WMPCore_AddRef,
    WMPCore_Release,
    WMPCore_GetTypeInfoCount,
    WMPCore_GetTypeInfo,
    WMPCore_GetIDsOfNames,
    WMPCore_Invoke,
    WMPCore_close,
    WMPCore_get_URL,
    WMPCore_put_URL,
    WMPCore_get_openState,
    WMPCore_get_playState,
    WMPCore_get_controls,
    WMPCore_get_settings,
    WMPCore_get_currentMedia,
    WMPCore_put_currentMedia,
    WMPCore_get_mediaCollection,
    WMPCore_get_playlistCollection,
    WMPCore_get_versionInfo,
    WMPCore_launchURL,
    WMPCore_get_network,
    WMPCore_get_currentPlaylist,
    WMPCore_put_currentPlaylist,
    WMPCore_get_cdromCollection,
    WMPCore_get_closedCaption,
    WMPCore_get_isOnline,
    WMPCore_get_error,
    WMPCore_get_status
};

#undef CORE_THIS


/* Implementation of IWMPPlayer interface */

#define PLAYER_THIS(iface) DEFINE_THIS(Player, WMPPlayer, iface)

    /*** IUnknown methods ***/
static HRESULT WINAPI WMPPlayer_QueryInterface(
        IWMPPlayer* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = PLAYER_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI WMPPlayer_AddRef(
                                          IWMPPlayer* iface)
{
    Player *This = PLAYER_THIS(iface);
    return IWMPCore_AddRef(CORE(This));
}

static ULONG WINAPI WMPPlayer_Release(
        IWMPPlayer* iface)
{
    Player *This = PLAYER_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

   /*** IDispatch methods ***/
static HRESULT WINAPI WMPPlayer_GetTypeInfoCount(
        IWMPPlayer* iface,
        UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


static HRESULT WINAPI WMPPlayer_GetTypeInfo(
        IWMPPlayer* iface,
        UINT iTInfo,
        LCID lcid,
        ITypeInfo **ppTInfo)
{
    TRACE("\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer_GetIDsOfNames(
        IWMPPlayer* iface,
        REFIID riid,
        LPOLESTR *rgszNames,
        UINT cNames,
        LCID lcid,
        DISPID *rgDispId)
{
    Player *This = PLAYER_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}

static HRESULT WINAPI WMPPlayer_Invoke(
        IWMPPlayer* iface,
        DISPID dispIdMember,
        REFIID riid,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pDispParams,
        VARIANT *pVarResult,
        EXCEPINFO *pExcepInfo,
        UINT *puArgErr)
{
    Player *This = PLAYER_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPCore methods ***/
static HRESULT WINAPI WMPPlayer_close(
             IWMPPlayer* iface)
{
    TRACE("\n");
    return WMPCore_close((IWMPCore*) iface);
}

static HRESULT WINAPI WMPPlayer_get_URL(
             IWMPPlayer* iface,
             BSTR *pbstrURL)
{
    TRACE("\n");
    return WMPCore_get_URL((IWMPCore*) iface, pbstrURL);
}

static HRESULT WINAPI WMPPlayer_put_URL(
             IWMPPlayer* iface,
             BSTR bstrURL)
{
    TRACE("\n");
    return WMPCore_put_URL((IWMPCore*) iface, bstrURL);
}

static HRESULT WINAPI WMPPlayer_get_openState(
             IWMPPlayer* iface,
             WMPOpenState *pwmpos)
{
    TRACE("\n");
    return WMPCore_get_openState((IWMPCore*) iface, pwmpos);
}

static HRESULT WINAPI WMPPlayer_get_playState(
             IWMPPlayer* iface,
             WMPPlayState *pwmpps)
{
    TRACE("\n");
    return WMPCore_get_playState((IWMPCore*) iface, pwmpps);
}

static HRESULT WINAPI WMPPlayer_get_controls(
             IWMPPlayer* iface,
             IWMPControls **ppControl)
{
    TRACE("\n");
    return WMPCore_get_controls((IWMPCore*) iface, ppControl);
}

static HRESULT WINAPI WMPPlayer_get_settings(
             IWMPPlayer* iface,
             IWMPSettings **ppSettings)
{
    TRACE("\n");
    return WMPCore_get_settings((IWMPCore*) iface, ppSettings);
}

static HRESULT WINAPI WMPPlayer_get_currentMedia(
             IWMPPlayer* iface,
             IWMPMedia **ppMedia)
{
    TRACE("\n");
    return WMPCore_get_currentMedia((IWMPCore*) iface, ppMedia);
}

static HRESULT WINAPI WMPPlayer_put_currentMedia(
             IWMPPlayer* iface,
             IWMPMedia *pMedia)
{
    TRACE("\n");
    return WMPCore_put_currentMedia((IWMPCore*) iface, pMedia);
}

static HRESULT WINAPI WMPPlayer_get_mediaCollection(
             IWMPPlayer* iface,
             IWMPMediaCollection **ppMediaCollection)
{
    TRACE("\n");
    return WMPCore_get_mediaCollection((IWMPCore*) iface, ppMediaCollection);
}

static HRESULT WINAPI WMPPlayer_get_playlistCollection(
             IWMPPlayer* iface,
             IWMPPlaylistCollection **ppPlaylistCollection)
{
    TRACE("\n");
    return WMPCore_get_playlistCollection((IWMPCore*) iface, ppPlaylistCollection);
}

static HRESULT WINAPI WMPPlayer_get_versionInfo(
             IWMPPlayer* iface,
             BSTR *pbstrVersionInfo)
{
    TRACE("\n");
    return WMPCore_get_versionInfo((IWMPCore*) iface, pbstrVersionInfo);
}

static HRESULT WINAPI WMPPlayer_launchURL(
             IWMPPlayer* iface,
             BSTR bstrURL)
{
    TRACE("\n");
    return WMPCore_launchURL((IWMPCore*) iface, bstrURL);
}

static HRESULT WINAPI WMPPlayer_get_network(
             IWMPPlayer* iface,
             IWMPNetwork **ppQNI)
{
    TRACE("\n");
    return WMPCore_get_network((IWMPCore*) iface, ppQNI);
}

static HRESULT WINAPI WMPPlayer_get_currentPlaylist(
             IWMPPlayer* iface,
             IWMPPlaylist **ppPL)
{
    TRACE("\n");
    return WMPCore_get_currentPlaylist((IWMPCore*) iface, ppPL);
}

static HRESULT WINAPI WMPPlayer_put_currentPlaylist(
             IWMPPlayer* iface,
             IWMPPlaylist *pPL)
{
    TRACE("\n");
    return WMPCore_put_currentPlaylist((IWMPCore*) iface, pPL);
}

static HRESULT WINAPI WMPPlayer_get_cdromCollection(
             IWMPPlayer* iface,
             IWMPCdromCollection **ppCdromCollection)
{
    TRACE("\n");
    return WMPCore_get_cdromCollection((IWMPCore*) iface, ppCdromCollection);
}

static HRESULT WINAPI WMPPlayer_get_closedCaption(
             IWMPPlayer* iface,
             IWMPClosedCaption **ppClosedCaption)
{
    TRACE("\n");
    return WMPCore_get_closedCaption((IWMPCore*) iface, ppClosedCaption);
}

static HRESULT WINAPI WMPPlayer_get_isOnline(
             IWMPPlayer* iface,
             VARIANT_BOOL *pfOnline)
{
    TRACE("\n");
    return WMPCore_get_isOnline((IWMPCore*) iface, pfOnline);
}

static HRESULT WINAPI WMPPlayer_get_error(
             IWMPPlayer* iface,
             IWMPError **ppError)
{
    TRACE("\n");
    return WMPCore_get_error((IWMPCore*) iface, ppError);
}

static HRESULT WINAPI WMPPlayer_get_status(
             IWMPPlayer* iface,
             BSTR *pbstrStatus)
{
    TRACE("\n");
    return WMPCore_get_status((IWMPCore*) iface, pbstrStatus);
}

    /*** IWMPPlayer methods ***/
static HRESULT WINAPI WMPPlayer_get_enabled(
             IWMPPlayer* This,
             VARIANT_BOOL *pbEnabled)
{
    TRACE("\n");
    *pbEnabled = VARIANT_TRUE;

    return S_OK;
}

static HRESULT WINAPI WMPPlayer_put_enabled(
             IWMPPlayer* This,
             VARIANT_BOOL bEnabled)
{
    TRACE("\n");
    return S_OK;
}

static HRESULT WINAPI WMPPlayer_get_fullScreen(
             IWMPPlayer* This,
             VARIANT_BOOL *pbFullScreen)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer_put_fullScreen(
             IWMPPlayer* This,
             VARIANT_BOOL bFullScreen)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer_get_enableContextMenu(
             IWMPPlayer* This,
             VARIANT_BOOL *pbEnableContextMenu)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer_put_enableContextMenu(
             IWMPPlayer* This,
             VARIANT_BOOL bEnableContextMenu)
{
    TRACE("stub, returns S_OK\n");
    return S_OK;
}

static HRESULT WINAPI WMPPlayer_put_uiMode(
             IWMPPlayer* This,
             BSTR bstrMode)
{
    TRACE("must be \"%s\", stub\n",debugstr_w(bstrMode));
    return S_OK;
}

static HRESULT WINAPI WMPPlayer_get_uiMode(
             IWMPPlayer* This,
             BSTR *pbstrMode)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}


const IWMPPlayerVtbl WMPPlayerVtbl =
{
    WMPPlayer_QueryInterface,
    WMPPlayer_AddRef,
    WMPPlayer_Release,
    WMPPlayer_GetTypeInfoCount,
    WMPPlayer_GetTypeInfo,
    WMPPlayer_GetIDsOfNames,
    WMPPlayer_Invoke,
    WMPPlayer_close,
    WMPPlayer_get_URL,
    WMPPlayer_put_URL,
    WMPPlayer_get_openState,
    WMPPlayer_get_playState,
    WMPPlayer_get_controls,
    WMPPlayer_get_settings,
    WMPPlayer_get_currentMedia,
    WMPPlayer_put_currentMedia,
    WMPPlayer_get_mediaCollection,
    WMPPlayer_get_playlistCollection,
    WMPPlayer_get_versionInfo,
    WMPPlayer_launchURL,
    WMPPlayer_get_network,
    WMPPlayer_get_currentPlaylist,
    WMPPlayer_put_currentPlaylist,
    WMPPlayer_get_cdromCollection,
    WMPPlayer_get_closedCaption,
    WMPPlayer_get_isOnline,
    WMPPlayer_get_error,
    WMPPlayer_get_status,
    WMPPlayer_get_enabled,
    WMPPlayer_put_enabled,
    WMPPlayer_get_fullScreen,
    WMPPlayer_put_fullScreen,
    WMPPlayer_get_enableContextMenu,
    WMPPlayer_put_enableContextMenu,
    WMPPlayer_put_uiMode,
    WMPPlayer_get_uiMode
};

#undef PLAYER_THIS


/* IWMPPlayer2 implementation */

#define PLAYER2_THIS(iface) DEFINE_THIS(Player, WMPPlayer2, iface)

static HRESULT WINAPI WMPPlayer2_QueryInterface(
             IWMPPlayer2* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = PLAYER2_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI WMPPlayer2_AddRef(
           IWMPPlayer2* iface)
{
    Player *This = PLAYER2_THIS(iface);
    return IWMPCore_AddRef(CORE(This));
}

static ULONG WINAPI WMPPlayer2_Release(
           IWMPPlayer2* iface)
{
    Player *This = PLAYER2_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI WMPPlayer2_GetTypeInfoCount(
             IWMPPlayer2* iface,
             UINT *pctinfo)
{
    return WMPPlayer_GetTypeInfoCount((IWMPPlayer*) iface, pctinfo);
}

static HRESULT WINAPI WMPPlayer2_GetTypeInfo(
             IWMPPlayer2* iface,
             UINT iTInfo,
             LCID lcid,
             ITypeInfo **ppTInfo)
{
    return WMPPlayer_GetTypeInfo((IWMPPlayer*) iface, iTInfo, lcid, ppTInfo);
}

static HRESULT WINAPI WMPPlayer2_GetIDsOfNames(
             IWMPPlayer2* iface,
             REFIID riid,
             LPOLESTR *rgszNames,
             UINT cNames,
             LCID lcid,
             DISPID *rgDispId)
{
    Player *This = PLAYER2_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}

static HRESULT WINAPI WMPPlayer2_Invoke(
             IWMPPlayer2* iface,
             DISPID dispIdMember,
             REFIID riid,
             LCID lcid,
             WORD wFlags,
             DISPPARAMS *pDispParams,
             VARIANT *pVarResult,
             EXCEPINFO *pExcepInfo,
             UINT *puArgErr)
{
    Player *This = PLAYER2_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPCore methods ***/
static HRESULT WINAPI WMPPlayer2_close(
             IWMPPlayer2* iface)
{
    return WMPPlayer_close((IWMPPlayer*) iface);
}

static HRESULT WINAPI WMPPlayer2_get_URL(
             IWMPPlayer2* iface,
             BSTR *pbstrURL)
{
    return WMPPlayer_get_URL((IWMPPlayer*) iface, pbstrURL);
}

static HRESULT WINAPI WMPPlayer2_put_URL(
             IWMPPlayer2* iface,
             BSTR bstrURL)
{
    return WMPPlayer_put_URL((IWMPPlayer*) iface, bstrURL);
}

static HRESULT WINAPI WMPPlayer2_get_openState(
             IWMPPlayer2* iface,
             WMPOpenState *pwmpos)
{
    return WMPPlayer_get_openState((IWMPPlayer*) iface, pwmpos);
}

static HRESULT WINAPI WMPPlayer2_get_playState(
             IWMPPlayer2* iface,
             WMPPlayState *pwmpps)
{
    return WMPPlayer_get_playState((IWMPPlayer*) iface, pwmpps);
}

static HRESULT WINAPI WMPPlayer2_get_controls(
             IWMPPlayer2* iface,
             IWMPControls **ppControl)
{
    return WMPPlayer_get_controls((IWMPPlayer*) iface, ppControl);
}

static HRESULT WINAPI WMPPlayer2_get_settings(
             IWMPPlayer2* iface,
             IWMPSettings **ppSettings)
{
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer2_get_currentMedia(
             IWMPPlayer2* iface,
             IWMPMedia **ppMedia)
{
    return WMPPlayer_get_currentMedia((IWMPPlayer*) iface, ppMedia);
}

static HRESULT WINAPI WMPPlayer2_put_currentMedia(
             IWMPPlayer2* iface,
             IWMPMedia *pMedia)
{
    return WMPPlayer_put_currentMedia((IWMPPlayer*) iface, pMedia);
}

static HRESULT WINAPI WMPPlayer2_get_mediaCollection(
             IWMPPlayer2* iface,
             IWMPMediaCollection **ppMediaCollection)
{
    return WMPPlayer_get_mediaCollection((IWMPPlayer*) iface, ppMediaCollection);
}

static HRESULT WINAPI WMPPlayer2_get_playlistCollection(
             IWMPPlayer2* iface,
             IWMPPlaylistCollection **ppPlaylistCollection)
{
    return WMPPlayer_get_playlistCollection((IWMPPlayer*) iface, ppPlaylistCollection);
}

static HRESULT WINAPI WMPPlayer2_get_versionInfo(
             IWMPPlayer2* iface,
             BSTR *pbstrVersionInfo)
{
    return WMPPlayer_get_versionInfo((IWMPPlayer*) iface, pbstrVersionInfo);
}

static HRESULT WINAPI WMPPlayer2_launchURL(
             IWMPPlayer2* iface,
             BSTR bstrURL)
{
    return WMPPlayer_launchURL((IWMPPlayer*) iface, bstrURL);
}

static HRESULT WINAPI WMPPlayer2_get_network(
             IWMPPlayer2* iface,
             IWMPNetwork **ppQNI)
{
    return WMPPlayer_get_network((IWMPPlayer*) iface, ppQNI);
}

static HRESULT WINAPI WMPPlayer2_get_currentPlaylist(
             IWMPPlayer2* iface,
             IWMPPlaylist **ppPL)
{
    return WMPPlayer_get_currentPlaylist((IWMPPlayer*) iface, ppPL);
}

static HRESULT WINAPI WMPPlayer2_put_currentPlaylist(
             IWMPPlayer2* iface,
             IWMPPlaylist *pPL)
{
    return WMPPlayer_put_currentPlaylist((IWMPPlayer*) iface, pPL);
}

static HRESULT WINAPI WMPPlayer2_get_cdromCollection(
             IWMPPlayer2* iface,
             IWMPCdromCollection **ppCdromCollection)
{
    return WMPPlayer_get_cdromCollection((IWMPPlayer*) iface, ppCdromCollection);
}

static HRESULT WINAPI WMPPlayer2_get_closedCaption(
             IWMPPlayer2* iface,
             IWMPClosedCaption **ppClosedCaption)
{
    return WMPPlayer_get_closedCaption((IWMPPlayer*) iface, ppClosedCaption);
}

static HRESULT WINAPI WMPPlayer2_get_isOnline(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pfOnline)
{
    return WMPPlayer_get_isOnline((IWMPPlayer*) iface, pfOnline);
}

static HRESULT WINAPI WMPPlayer2_get_error(
             IWMPPlayer2* iface,
             IWMPError **ppError)
{
    return WMPPlayer_get_error((IWMPPlayer*) iface, ppError);
}

static HRESULT WINAPI WMPPlayer2_get_status(
             IWMPPlayer2* iface,
             BSTR *pbstrStatus)
{
    return WMPPlayer_get_status((IWMPPlayer*) iface, pbstrStatus);
}

    /*** IWMPPlayer2 methods ***/
static HRESULT WINAPI WMPPlayer2_get_enabled(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pbEnabled)
{
    return WMPPlayer_get_enabled((IWMPPlayer*) iface, pbEnabled);
}

static HRESULT WINAPI WMPPlayer2_put_enabled(
             IWMPPlayer2* iface,
             VARIANT_BOOL bEnabled)
{
    return WMPPlayer_put_enabled((IWMPPlayer*) iface, bEnabled);
}

static HRESULT WINAPI WMPPlayer2_get_fullScreen(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pbFullScreen)
{
    return WMPPlayer_get_fullScreen((IWMPPlayer*) iface, pbFullScreen);
}

static HRESULT WINAPI WMPPlayer2_put_fullScreen(
             IWMPPlayer2* iface,
             VARIANT_BOOL bFullScreen)
{
    return WMPPlayer_put_fullScreen((IWMPPlayer*) iface, bFullScreen);
}

static HRESULT WINAPI WMPPlayer2_get_enableContextMenu(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pbEnableContextMenu)
{
    return WMPPlayer_get_enableContextMenu((IWMPPlayer*) iface, pbEnableContextMenu);
}

static HRESULT WINAPI WMPPlayer2_put_enableContextMenu(
             IWMPPlayer2* iface,
             VARIANT_BOOL bEnableContextMenu)
{
    return WMPPlayer_put_enableContextMenu((IWMPPlayer*) iface, bEnableContextMenu);
}

static HRESULT WINAPI WMPPlayer2_put_uiMode(
             IWMPPlayer2* iface,
             BSTR bstrMode)
{
    return WMPPlayer_put_uiMode((IWMPPlayer*) iface, bstrMode);
}

static HRESULT WINAPI WMPPlayer2_get_uiMode(
             IWMPPlayer2* iface,
             BSTR *pbstrMode)
{
    return WMPPlayer_get_uiMode((IWMPPlayer*) iface, pbstrMode);
}

static HRESULT WINAPI WMPPlayer2_get_stretchToFit(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pbEnabled)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer2_put_stretchToFit(
             IWMPPlayer2* iface,
             VARIANT_BOOL bEnabled)
{
    TRACE("stub, returns S_OK\n");
    return S_OK;
}

static HRESULT WINAPI WMPPlayer2_get_windowlessVideo(
             IWMPPlayer2* iface,
             VARIANT_BOOL *pbEnabled)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPPlayer2_put_windowlessVideo(
             IWMPPlayer2* iface,
             VARIANT_BOOL bEnabled)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

const IWMPPlayer2Vtbl WMPPlayer2Vtbl =
{
    WMPPlayer2_QueryInterface,
    WMPPlayer2_AddRef,
    WMPPlayer2_Release,
    WMPPlayer2_GetTypeInfoCount,
    WMPPlayer2_GetTypeInfo,
    WMPPlayer2_GetIDsOfNames,
    WMPPlayer2_Invoke,
    WMPPlayer2_close,
    WMPPlayer2_get_URL,
    WMPPlayer2_put_URL,
    WMPPlayer2_get_openState,
    WMPPlayer2_get_playState,
    WMPPlayer2_get_controls,
    WMPPlayer2_get_settings,
    WMPPlayer2_get_currentMedia,
    WMPPlayer2_put_currentMedia,
    WMPPlayer2_get_mediaCollection,
    WMPPlayer2_get_playlistCollection,
    WMPPlayer2_get_versionInfo,
    WMPPlayer2_launchURL,
    WMPPlayer2_get_network,
    WMPPlayer2_get_currentPlaylist,
    WMPPlayer2_put_currentPlaylist,
    WMPPlayer2_get_cdromCollection,
    WMPPlayer2_get_closedCaption,
    WMPPlayer2_get_isOnline,
    WMPPlayer2_get_error,
    WMPPlayer2_get_status,
    WMPPlayer2_get_enabled,
    WMPPlayer2_put_enabled,
    WMPPlayer2_get_fullScreen,
    WMPPlayer2_put_fullScreen,
    WMPPlayer2_get_enableContextMenu,
    WMPPlayer2_put_enableContextMenu,
    WMPPlayer2_put_uiMode,
    WMPPlayer2_get_uiMode,
    WMPPlayer2_get_stretchToFit,
    WMPPlayer2_put_stretchToFit,
    WMPPlayer2_get_windowlessVideo,
    WMPPlayer2_put_windowlessVideo
};

#undef PLAYER2_THIS

extern HRESULT Player_Create(IUnknown *pUnkOuter, REFIID riid, void** ppvObject)
{
    Player *ret;
    HRESULT hres;

    TRACE("(%p %s %p)\n", pUnkOuter, debugstr_guid(riid), ppvObject);

    ret = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(Player));

    ret->lpWMPCoreVtbl = &WMPCoreVtbl;
    ret->lpWMPPlayerVtbl = &WMPPlayerVtbl;
    ret->lpWMPPlayer2Vtbl = &WMPPlayer2Vtbl;

    Player_DispatchEx_Init(ret);
    Player_OleObj_Init(ret);
    Player_WMPControls_Init(ret);
    Player_WMPSettings_Init(ret);
    Player_WMPError_Init(ret);
    Player_MediaPlayer_Init(ret);
    Player_Persist_Init(ret);
    Player_PerProperty_Init(ret);
    Player_PointerInactive_Init(ret);
    Player_Connection_Init(ret);
    Player_Safety_Init(ret);
    Player_RunObj_Init(ret);
    Player_Security_Init(ret);
    Player_ViewObj_Init(ret);
    Player_OleWindow_Init(ret);

    ret->ref = 0;

    ret->client = NULL;
    ret->sizel.cx = 0;
    ret->sizel.cy = 0;
    ret->cpoint = CPOINT(ret);
    ret->playState = wmppsUndefined;
    ret->url[0] = '\0';
    ret->baseurl[0] = '\0';
    ret->CurrentPosition = 0;
    ret->FileName[0] = '\0';
    ret->DisplayMode = 0;
    ret->Duration = 0;

    hres = IWMPCore_QueryInterface(CORE(ret), riid, ppvObject);
    if(FAILED(hres)) {
        HeapFree(GetProcessHeap(), 0, ret);
        return hres;
    }

    InterlockedIncrement(&dll_ref);

    return hres;
}
