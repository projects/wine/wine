/*
 * Main header file
 *
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WMP_H__
#define __WMP_H__

#include <stdarg.h>
#include <stdio.h>

#define COBJMACROS

#include <windef.h>
#include <winbase.h>
#include <winuser.h>
#include <winreg.h>
#include <ole2.h>
#include <oleidl.h>
#include <ocidl.h>
#include <docobj.h>
#include <dispex.h>
#include <objsafe.h>
#include "wmp.h"
#include <wine/debug.h>

WINE_DEFAULT_DEBUG_CHANNEL (wmp);

/* debug.h defines it only for internal compilation */
#ifndef __WINESRC__
#define TRACE WINE_TRACE
#define FIXME WINE_FIXME
extern char *debugstr_w(LPCWSTR str);
#endif

extern LONG dll_ref;

#define DISPATCHEX(x)           ((IDispatchEx*)         &(x)->lpDispatchExVtbl)
#define CORE(x)                 ((IWMPCore*)            &(x)->lpWMPCoreVtbl)
#define PLAYER(x)               ((IWMPPlayer*)          &(x)->lpWMPPlayerVtbl)
#define PLAYER2(x)              ((IWMPPlayer2*)         &(x)->lpWMPPlayer2Vtbl)
#define CONTROLS(x)             ((IWMPControls*)        &(x)->lpWMPControlsVtbl)
#define SETTINGS(x)             ((IWMPSettings*)        &(x)->lpWMPSettingsVtbl)
#define WMPERROR(x)             ((IWMPError*)           &(x)->lpWMPErrorVtbl)
#define MEDIAPLAYER(x)          ((IMediaPlayer*)        &(x)->lpMediaPlayerVtbl)
#define OLEOBJ(x)               ((IOleObject*)          &(x)->lpOleObjectVtbl)
#define CONTROL(x)              ((IOleControl*)         &(x)->lpOleControlVtbl)
#define CMDTARGET(x)            ((IOleCommandTarget*)   &(x)->lpOleCommandTargetVtbl)
#define PERSIST(x)              ((IPersist*)            &(x)->lpPersistVtbl)
#define PERSISTSTREAM(x)        ((IPersistStream*)      &(x)->lpPersistStreamVtbl)
#define PERSISTSTREAMINIT(x)    ((IPersistStreamInit*)  &(x)->lpPersistStreamInitVtbl)
#define PROPBAG(x)              ((IPersistPropertyBag*)    &(x)->lpPersistPropertyBagVtbl)
#define PROPBAG2(x)             ((IPersistPropertyBag2*)   &(x)->lpPersistPropertyBag2Vtbl)
#define PROPBROWSING(x)         ((IPerPropertyBrowsing*)    &(x)->lpPerPropertyBrowsingVtbl)
#define HISTORY(x)              ((IPersistHistory*)         &(x)->lpPersistHistoryVtbl)
#define CPCONT(x)               ((IConnectionPointContainer*) &(x)->lpConnectionPointContainerVtbl)
#define CPOINT(x)               ((IConnectionPoint*)    &(x)->lpConnectionPointVtbl)
#define INACTIVE(x)             ((IPointerInactive*)    &(x)->lpPointerInactiveVtbl)
#define SAFETY(x)               ((IObjectSafety*)       &(x)->lpObjectSafetyVtbl)
#define RUNOBJ(x)               ((IRunnableObject*)     &(x)->lpRunnableObjectVtbl)
#define VIEWOBJ(x)              ((IViewObject*)         &(x)->lpViewObjectVtbl)
#define VIEWOBJEX(x)            ((IViewObjectEx*)       &(x)->lpViewObjectExVtbl)
#define SECURITY(x)             ((IClientSecurity*)     &(x)->lpClientSecurityVtbl)
#define OLEWINDOW(x)            ((IOleWindow*)          &(x)->lpOleWindowVtbl)
#define INPLACE(x)              ((IOleInPlaceObject*)   &(x)->lpOleInPlaceObjectVtbl)
#define INPLACEW(x)             ((IOleInPlaceObjectWindowless*)  &(x)->lpOleInPlaceObjectWindowlessVtbl)

typedef struct
{
    const IClassFactoryVtbl *lpVtbl;
    LONG ref;
} ClassFactoryImpl;

typedef struct
{
    const IDispatchExVtbl                  *lpDispatchExVtbl;
    const IWMPCoreVtbl                     *lpWMPCoreVtbl;
    const IWMPPlayerVtbl                   *lpWMPPlayerVtbl;
    const IWMPPlayer2Vtbl                  *lpWMPPlayer2Vtbl;
    const IWMPControlsVtbl                 *lpWMPControlsVtbl;
    const IWMPSettingsVtbl                 *lpWMPSettingsVtbl;
    const IWMPErrorVtbl                    *lpWMPErrorVtbl;
    const IMediaPlayerVtbl                 *lpMediaPlayerVtbl;
    const IOleObjectVtbl                   *lpOleObjectVtbl;
    const IOleControlVtbl                  *lpOleControlVtbl;
    const IOleCommandTargetVtbl            *lpOleCommandTargetVtbl;
    const IPersistVtbl                     *lpPersistVtbl;
    const IPersistStreamVtbl               *lpPersistStreamVtbl;
    const IPersistStreamInitVtbl           *lpPersistStreamInitVtbl;
    const IPersistPropertyBagVtbl          *lpPersistPropertyBagVtbl;
    const IPersistPropertyBag2Vtbl         *lpPersistPropertyBag2Vtbl;
    const IPerPropertyBrowsingVtbl         *lpPerPropertyBrowsingVtbl;
    const IPersistHistoryVtbl              *lpPersistHistoryVtbl;
    const IConnectionPointContainerVtbl    *lpConnectionPointContainerVtbl;
    const IConnectionPointVtbl             *lpConnectionPointVtbl;
    const IPointerInactiveVtbl             *lpPointerInactiveVtbl;
    const IObjectSafetyVtbl                *lpObjectSafetyVtbl;
    const IRunnableObjectVtbl              *lpRunnableObjectVtbl;
    const IViewObjectVtbl                  *lpViewObjectVtbl;
    const IViewObjectExVtbl                *lpViewObjectExVtbl;
    const IClientSecurityVtbl              *lpClientSecurityVtbl;
    const IOleWindowVtbl                   *lpOleWindowVtbl;
    const IOleInPlaceObjectVtbl            *lpOleInPlaceObjectVtbl;
    const IOleInPlaceObjectWindowlessVtbl  *lpOleInPlaceObjectWindowlessVtbl;

    LONG ref;

    IOleClientSite *client;
    SIZEL sizel;
    IConnectionPoint *cpoint;
    WMPPlayState playState;
    WCHAR url[MAX_PATH];
    WCHAR baseurl[MAX_PATH];
    DOUBLE CurrentPosition;
    WCHAR FileName[MAX_PATH];
    MPDisplayModeConstants DisplayMode;
    double Duration;
} Player;

void Player_DispatchEx_Init(Player*);
void Player_OleObj_Init(Player*);
void Player_WMPControls_Init(Player*);
void Player_WMPSettings_Init(Player*);
void Player_WMPError_Init(Player*);
void Player_MediaPlayer_Init(Player*);
void Player_Persist_Init(Player*);
void Player_PerProperty_Init(Player*);
void Player_PointerInactive_Init(Player*);
void Player_Connection_Init(Player*);
void Player_Safety_Init(Player*);
void Player_RunObj_Init(Player*);
void Player_Security_Init(Player*);
void Player_ViewObj_Init(Player*);
void Player_OleWindow_Init(Player*);

#define DEFINE_THIS2(cls,ifc,iface) ((cls*)((BYTE*)(iface)-offsetof(cls,ifc)))
#define DEFINE_THIS(cls,ifc,iface) DEFINE_THIS2(cls,lp ## ifc ## Vtbl,iface)

extern ClassFactoryImpl classfactory;
extern HRESULT Player_Create(IUnknown *pUnkOuter, REFIID riid, void** ppvObject);
extern HRESULT WMPCore_Create(IUnknown *pUnkOuter, REFIID riid, void** ppvObject);

#endif /* __WMP_H__ */
