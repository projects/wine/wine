#include <unistd.h>
#include <stdio.h>
#include "wine/server.h"
#include "wine/server_protocol.h"

#define CACHE_MAX_NUM 300

struct CacheInfo
{
	void* handle;
	struct __server_request_info reply;
	ULONG	ms;
	thread_id_t  tid; // use for get_thread_input_request
};

struct CacheInfo mcache_win_tree[CACHE_MAX_NUM];
struct CacheInfo mcache_win_child[CACHE_MAX_NUM];
struct CacheInfo mcache_get_thread[CACHE_MAX_NUM];

static int win_tree_curindex = 0;
static int win_child_curindex = 0;
static int get_thread_curindex = 0;
static int win_tree_empty = 1;
static int win_child_empty = 1;
static int get_thread_empty = 1;
static int cache_clear = 0;

static const int get_window_children_timeout_msec = 1000;
static const int get_window_tree_timeout_msec = 200;
static const int get_thread_input_timeout_msec = 200;

static int mcache_init_ok = 0;

// -----------------------------------------------------------------------------
void mcache_init()
{
	if( mcache_init_ok )
		return;
		
	memset(mcache_win_tree,0,sizeof(mcache_win_tree));
	memset(mcache_win_child,0,sizeof(mcache_win_child));
	memset(mcache_get_thread,0,sizeof(mcache_get_thread));
	
	mcache_init_ok = 1;
}

// -----------------------------------------------------------------------------
static BOOL mcache_compare_win_tree( int ind,  const struct __server_request_info* req )
{
	struct get_window_tree_request* r = (struct get_window_tree_request*)(&req->u.req);
	return ( mcache_win_tree[ind].handle==r->handle );
}
// -----------------------------------------------------------------------------
static BOOL mcache_compare_win_child( int ind,  const struct __server_request_info* req )
{
	struct get_window_children_request* r = (struct get_window_children_request*)(&req->u.req);
	return ( mcache_win_child[ind].handle==r->parent );
}
// -----------------------------------------------------------------------------
static BOOL mcache_compare_get_thread( int ind,  const struct __server_request_info* req )
{
	struct get_thread_input_request* r = (struct get_thread_input_request*)(&req->u.req);
	return ( mcache_get_thread[ind].tid==r->tid );
}
// -----------------------------------------------------------------------------

static BOOL mcache_filtr( const struct __server_request_info* req )
{
//	return 0;
	
	switch( req->u.req.request_header.req )
	{
		case REQ_get_window_tree:
//		case REQ_get_window_children:
		case REQ_get_thread_input:
		break;

		// ��������� ��������� ������� ����
		case REQ_set_window_owner:
		case REQ_set_window_pos:
		case REQ_set_window_region:
		case REQ_set_window_property:
		case REQ_set_foreground_window:
		case REQ_set_focus_window:
		case REQ_set_active_window:
		case REQ_set_capture_window:
		case REQ_set_caret_window:
		case REQ_set_global_windows:
			cache_clear = 1;
		return 0;

		case REQ_set_window_info:
		{
			struct set_window_info_request* r = (struct set_window_info_request*)(&req->u.req);
			if( r->flags )
				cache_clear = 1;
		}
		return 0;
	
		default:
			return 0;
	}

	return 1;	
}

// -----------------------------------------------------------------------------
static void mcache_clear()
{
	if( win_child_empty && win_tree_empty && get_thread_empty )
		return;

	memset(mcache_win_tree,0,sizeof(mcache_win_tree));
	memset(mcache_win_child,0,sizeof(mcache_win_child));
	memset(mcache_get_thread,0,sizeof(mcache_get_thread));
	win_tree_curindex 	= 0;
	win_child_curindex 	= 0;
	get_thread_curindex = 0;
	win_tree_empty 		= 1;
	win_child_empty 	= 1;
	get_thread_empty 	= 1;
}
// -----------------------------------------------------------------------------
// ���������� ��������� '�����'
static BOOL mcache_check( struct CacheInfo* mcache, 
							BOOL (*mcache_compare)( int ind,  const struct __server_request_info* req ),
							const struct __server_request_info* req, 
							struct __server_request_info* rep, int* curindex, int cache_timeout );
// -----------------------------------------------------------------------------
static BOOL mcache_get_win_tree( const struct __server_request_info* req, struct __server_request_info* rep )
{
	return mcache_check( mcache_win_tree,mcache_compare_win_tree,req,rep, 
							&win_tree_curindex, get_window_tree_timeout_msec );
}
// -----------------------------------------------------------------------------
static BOOL mcache_get_win_child( const struct __server_request_info* req, struct __server_request_info* rep )
{
	return mcache_check( mcache_win_child,mcache_compare_win_child,req,rep, 
							&win_child_curindex, get_window_children_timeout_msec );
}
// -----------------------------------------------------------------------------
static BOOL mcache_get_thread_input( const struct __server_request_info* req, struct __server_request_info* rep )
{
	return mcache_check( mcache_get_thread,mcache_compare_get_thread,req,rep, 
							&get_thread_curindex,get_thread_input_timeout_msec );
}
// -----------------------------------------------------------------------------

BOOL mcache_check_msg( const struct __server_request_info* req,
						struct __server_request_info* rep )
{
	if( !mcache_filtr(req) )
	{
		if( cache_clear )
		{
			mcache_clear();
			cache_clear = 0;
		}
		return 0;
	}

	switch( req->u.req.request_header.req )
	{
		case REQ_get_window_tree:
			return mcache_get_win_tree(req,rep);

		case REQ_get_window_children:
			return mcache_get_win_child(req,rep);

		case REQ_get_thread_input:
			return mcache_get_thread_input(req,rep);

		default:
			break;
	}

	return 0;

}
// -----------------------------------------------------------------------------
// ������� ���� � ���� ��� �������� ������� ��� ������ ��������� ����� ��� ���� (tm=0)
static int mcache_find_index( struct CacheInfo* mcache,
						BOOL (*mcache_compare)( int ind,  const struct __server_request_info* req ),
						struct __server_request_info* req )
{
	int i=0;
	int first = -1;
	for( ;i<CACHE_MAX_NUM; i++ )
	{
		if( mcache_compare(i,req) )
			return i;
		
		if( first < 0 && !mcache[i].ms )
			first = i;
	}

	return first;
}
// -----------------------------------------------------------------------------
static void mcache_update_win_tree( struct __server_request_info* req, struct __server_request_info* rep )
{
	struct get_window_tree_request* r = (struct get_window_tree_request*)(&req->u.req);

	int ind = mcache_find_index( mcache_win_tree, mcache_compare_win_tree, req );
	
	if( ind < 0 )
		ind = win_tree_curindex;

	mcache_win_tree[ind].handle 	= r->handle;
	mcache_win_tree[ind].ms 		= NtGetTickCount();
	mcache_win_tree[ind].reply 		= (*rep);
	win_tree_curindex++;
	if( win_tree_curindex >= CACHE_MAX_NUM )
		win_tree_curindex = 0;

	win_tree_empty = 0;

//	fprintf(stderr,"(mcache_update): add reply to cache for %p curindex=%d\n",r->handle,curindex);
}
// -----------------------------------------------------------------------------
static void mcache_update_win_child( struct __server_request_info* req, struct __server_request_info* rep )
{
	struct get_window_children_request* r = (struct get_window_children_request*)(&req->u.req);

	int ind = mcache_find_index( mcache_win_child, mcache_compare_win_child, req );
	if( ind < 0 )
		ind = win_child_curindex;

	mcache_win_child[ind].handle 	= r->parent;
	mcache_win_child[ind].ms 	= NtGetTickCount();
	mcache_win_child[ind].reply 	= (*rep);
	win_child_curindex++;
	if( win_child_curindex >= CACHE_MAX_NUM )
		win_child_curindex = 0;

	win_child_empty = 0;

//	fprintf(stderr,"(mcache_update): add reply to cache for %p curindex=%d\n",r->handle,curindex);
}
// -----------------------------------------------------------------------------
static void mcache_update_get_thread( struct __server_request_info* req, struct __server_request_info* rep )
{
	struct get_thread_input_request* r = (struct get_thread_input_request*)(&req->u.req);

	int ind = mcache_find_index( mcache_get_thread, mcache_compare_get_thread, req );
	
	if( ind < 0 )
		ind = get_thread_curindex;

	mcache_get_thread[ind].handle 	= 0;
	mcache_get_thread[ind].tid 	= r->tid;
	mcache_get_thread[ind].ms 	= NtGetTickCount();
	mcache_get_thread[ind].reply 	= *rep;
	get_thread_curindex++;
	if( get_thread_curindex >= CACHE_MAX_NUM )
		get_thread_curindex = 0;

	get_thread_empty = 0;

//	fprintf(stderr,"(mcache_update_get_thread): add reply to cache for %p tid_in=%d curindex=%d\n",
//						r->handle,r->tid_in,get_thread_curindex);
}
// -----------------------------------------------------------------------------

void mcache_update( struct __server_request_info* req, struct __server_request_info* rep )
{
	if( !mcache_init_ok )
		mcache_init();

	if( !mcache_filtr(req) )
		return;

	switch( req->u.req.request_header.req )
	{
		case REQ_get_window_tree:
			mcache_update_win_tree(req, rep);
		break;

		case REQ_get_window_children:
			mcache_update_win_child(req, rep);
		break;

		case REQ_get_thread_input:
			mcache_update_get_thread(req, rep);
		break;

		default:
			return;
	}
}
// -----------------------------------------------------------------------------
// ����ݣ���� ������ ����������� ������ � �����
// � �������� ���������� ��������� ������(���) � ������� ��ģ��� ������
// ��������� �� ������� ��������� (������ � ����)
// � ���������� ������...
static BOOL mcache_check( struct CacheInfo* mcache,
					BOOL (*mcache_compare)( int ind,  const struct __server_request_info* req ),
					const struct __server_request_info* req, struct __server_request_info* rep,
					int* curindex, int cache_timeout )
{
	int i 		= 0;
	BOOL old 	= 0;
	BOOL empty 	= 1;
	ULONG t2 	= NtGetTickCount();

	/* � ���� ����� ����� ����� �������� ��� ������ � ������� ��� �������!
	// ����� � ������� update, ���������� � ������ ���������(����������) ������
	// ����� ������ � ���, ��� ���� ��������� difftime �� ������ ���� (�� mcache_compare)
	// ���� ����� ����������� ������ ����� ����� ��������....
	*/
	for( ; i<CACHE_MAX_NUM; i++ )
	{
		if( !mcache[i].ms )
			continue;

		old = ((t2 - mcache[i].ms) >= cache_timeout );

		if( mcache_compare(i,req) )
		{
			if( !old )
			{
/*				fprintf(stderr,"(mcache_check): find in cache handle=%p\n",mcache[i].handle); */
				*rep = mcache[i].reply;
				return 1;
			}
		}
		
		if( old )
			mcache[i].ms = 0; /* ��������, ��� ����� �������� */
		else
			empty = 0;
	}

	if( empty )
		*curindex = 0;

	return 0;
}
