/*
 * WINESPLASH - Splash screen for Wine
 *
 * Copyright (C) 2008 Illarion Ishkulov <gentro@etersoft.ru (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/xpm.h>

typedef struct _splash Splash;

struct _splash
{
	Display* display;
	Window window;
	Window bar;
	int screen_number;
	int x;
	int y;
	int width;
	int heigth;
	Pixmap pic;
	XColor bar_color;
	Colormap colormap;
	unsigned int pic_width;
	unsigned int pic_height;
	int prog;
};

int create_splash(Splash* sp, char* filename, const char* color, int yposition );
int show_splash(Splash* sp);
int splash_set_bar(Splash* sp, int proc );
int remove_splash(Splash* sp);
void redraw(Splash* sp);
