#!/bin/sh

# Place device mapping here
# Called from wine script

################################################################

# Map D: to home dir (for winetricks) always
map_drive ~/ d:

# Map Y: to /usr/share (eterbug #5460,#2849)
#map_drive /usr/share y:

# Use root filesystem as WINE disk (not recommended)
# map_drive / z:

# Map mounted share dir
#[ -d /net/sharebase ] && map_drive /net/sharebase n:

# Map shared base dir (by default, as in the doc)
[ -d /var/local/sharebase ] && map_drive /var/local/sharebase s:

# Create UNC drive by default
#if [ ! -d "$DEV/unc/server/share" ] ; then
#	mkdir -p "$DEV/unc/server/share" && map_drive unc/server/share u: force
#fi
