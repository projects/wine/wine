#!/bin/sh

# Publish to git.etersoft.ru

git push --all $1 git.eter:/projects/eterwine.git
git push --tags git.eter:/projects/eterwine.git

PROJECT=wine
if [ "$USER" = "lav" ] ; then
	echo "Publish to ALT"
	git push git.alt:packages/$PROJECT.git HEAD:master
	git push --tags git.alt:packages/$PROJECT.git HEAD:master
fi
