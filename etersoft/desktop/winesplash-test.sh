#!/bin/sh

FILE=winesplash
echo 1 >$FILE.status

[ -r "$FILE.xpm.conf" ] && . $FILE.xpm.conf
SPLASH_TIMEOUT=10
../winesplash/winesplash $FILE.status $FILE.xpm "$SPLASH_COLOR" "$SPLASH_TIMEOUT" "$SPLASH_YPOSITION" &

for i in $(seq 1 89) ; do
	echo $i >$FILE.status
	sleep 20.1
done

sleep 3

rm -f $FILE.status

